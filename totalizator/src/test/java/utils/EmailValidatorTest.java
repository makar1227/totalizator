package utils;

import by.epam.makarevich.totalizator.util.EmailValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmailValidatorTest {

    @DataProvider
    public Object[][] validEmailProvider() {
        return new Object[][]{{new String[]{"makar@yahoo.com",
                "makar-100@yahoo.com", "makar.100@yahoo.com",
                "makar111@makar.com", "makar-100@makar.net",
                "makar.100@makar.com.au", "makar@1.com",
                "makar@gmail.com.com", "makar+100@gmail.com",
                "makar-100@yahoo-test.com"}}};
    }

    @DataProvider
    public Object[][] invalidEmailProvider() {
        return new Object[][]{{new String[]{"makar", "makar@.com.my",
                "makar123@gmail.a", "makar123@.com", "makar123@.com.com",
                ".makar@makar.com", "makar()*@gmail.com", "makar@%*.com",
                "makar..2002@gmail.com", "makar.@gmail.com",
                "makar@makar@gmail.com", "makar@gmail.com.1a"}}};
    }

    @Test(dataProvider = "validEmailProvider")
    public void validEmailTest(String[] Email) {

        for (String temp : Email) {
            boolean valid = EmailValidator.validateEmail(temp);
            Assert.assertEquals(valid, true);
        }

    }

    @Test(dataProvider = "invalidEmailProvider", dependsOnMethods = "validEmailTest")
    public void inValidEmailTest(String[] Email) {

        for (String temp : Email) {
            boolean valid = EmailValidator.validateEmail(temp);
            Assert.assertEquals(valid, false);
        }
    }
}
