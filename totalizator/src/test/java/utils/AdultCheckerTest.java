package utils;

import by.epam.makarevich.totalizator.util.AdultChecker;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AdultCheckerTest {
    @DataProvider
    public Object[][] validAgeProvider() {
        return new Object[][]{
                new Object[]{"1978-02-08"}, new Object[]{"1999-05-01"},
        };
    }

    @DataProvider
    public Object[][] invalidAgeProvider() {
        return new Object[][]{
                new Object[]{"2010-01-01"}, new Object[]{"2020-01-01"},
                new Object[]{"2000-01-31"},

        };
    }

    @Test(dataProvider = "validAgeProvider")
    public void validAgeTest(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        boolean valid = AdultChecker.checkAdult(d);
        Assert.assertEquals(true, valid);
    }

    @Test(dataProvider = "invalidAgeProvider",
            dependsOnMethods = "validAgeTest")
    public void inValidDateTest(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        boolean valid = AdultChecker.checkAdult(d);
        Assert.assertEquals(false, valid);
    }
}
