package utils;

import by.epam.makarevich.totalizator.util.PhoneValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class PhoneValidatorTest {

    @DataProvider
    public Object[][] validPhoneProvider() {
        return new Object[][]{{new String[]{"+37528-348-15-87","+25978-389-42-87",
                "+00000-000-00-00","+99962-348-83-99"}}};
    }

    @DataProvider
    public Object[][] invalidPhoneProvider() {
        return new Object[][]{{new String[]{"37528-348-15-87","+3528-348-15-87",
                "+37528-3a8-15-87","+37528-348-1-87",
                "+37528-348-15-877","-37528-348-15-87"
        }}};
    }

    @Test(dataProvider = "validPhoneProvider")
    public void validPhoneTest(String[] Phone) {

        for (String temp : Phone) {
            boolean valid = PhoneValidator.checkPhone(temp);
            Assert.assertEquals(valid, true);
        }

    }

    @Test(dataProvider = "invalidPhoneProvider", dependsOnMethods = "validPhoneTest")
    public void inValidPhoneTest(String[] Phone) {

        for (String temp : Phone) {
            boolean valid = PhoneValidator.checkPhone(temp);
            Assert.assertEquals(valid, false);
        }
    }
}
