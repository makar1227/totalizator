function profileFormValidation(form) {
    var result = true; // результат, используем, чтобы проверять сразу все поля ввода
// Константы
    var FILL_FIELD = STRINGS.error_empty_field,
        BAD_PASSWORD = STRINGS.error_incorrect_password,
        BAD_NEW_PASSWORD = STRINGS.error_incorrect_password,
        BAD_PHONE = STRINGS.error_incorrect_phone,
        BAD_EMAIL = STRINGS.error_incorrect_email;

// Находим ссылки на элементы сообщений об ошибках
    var

        errEmail = document.getElementById("err-email"),
        errPhone = document.getElementById("err-phone"),
        errPwd1 = document.getElementById("err-password"),
        errPwd2 = document.getElementById("err-newPassword");

// Читаем значения из полей формы
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var password =document.getElementById("password").value;
    var newPassword = document.getElementById("newPassword").value;

//email
    if (!email) {
        errEmail.innerHTML = FILL_FIELD;
        result = false;
    }
    var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if (email && !re.test(email)) {
        errEmail.innerHTML = BAD_EMAIL;
        result = false;
    }


//phone
    if (!phone) {
        errPhone.innerHTML = FILL_FIELD;
        result = false;
    }
    var reph = /\+[0-9]{5}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/;

    if (phone && !reph.test(phone)) {
        errPhone.innerHTML = BAD_PHONE;
        result = false;
    }


//password
    if (!password) {
        errPwd1.innerHTML = FILL_FIELD;
        result = false;
    }  // обязательное заполнение
    if (!newPassword) {
        errPwd2.innerHTML = FILL_FIELD;
        result = false;
    }  // обязательное заполнение

    var repsw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
    if (newPassword && !repsw.test(newPassword)) {
        errPwd1.innerHTML = BAD_NEW_PASSWORD;
        // document.getElementById("password").value = "";   // сброс
        document.getElementById("newPassword").value = "";   // сброс
        result = false;
    }

    return result;
}
