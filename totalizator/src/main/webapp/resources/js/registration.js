function registrationFormValidation(form) {
    var result = true; // результат, используем, чтобы проверять сразу все поля ввода
// Константы
    var FILL_FIELD = STRINGS.error_empty_field,
        BAD_LOGIN = STRINGS.error_incorrect_login,
        BAD_PASSWORD = STRINGS.error_incorrect_password,
        PWD_NOT_EQUAL = STRINGS.error_different_passwords,
        BAD_PHONE = STRINGS.error_incorrect_phone,
        UNDER18 = STRINGS.error_under_18,
        BAD_DATE=STRINGS.error_incorrect_age,
        BAD_EMAIL = STRINGS.error_incorrect_email;

// Находим ссылки на элементы сообщений об ошибках
    var

        errEmail = document.getElementById("err-email"),
        errPhone = document.getElementById("err-phone"),
        errLogin = document.getElementById("err-login"),
        errPwd1 = document.getElementById("err-password"),
        errPwd2 = document.getElementById("err-passwordConfirm"),
        errBday = document.getElementById("err-bday");


// Читаем значения из полей формы
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var login = document.getElementById("login").value;
    var password =document.getElementById("password").value;
    var confPassword = document.getElementById("passwordConfirm").value;
    var bday = document.getElementById("birth").value;


//email
    if (!email) {
        errEmail.innerHTML = FILL_FIELD;
        result = false;
    }
    var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if (email && !re.test(email)) {
        errEmail.innerHTML = BAD_EMAIL;
        result = false;
    }


//phone
    if (!phone) {
        errPhone.innerHTML = FILL_FIELD;
        result = false;
    }
    var reph = /\+[0-9]{5}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/;

    if (phone && !reph.test(phone)) {
        errPhone.innerHTML = BAD_PHONE;
        result = false;
    }

//login
    if (!login) {
        errLogin.innerHTML = FILL_FIELD;
        result = false;
    }

    var relog = /[A-Za-z]{1}\w{4,30}$/;
    if (login && !relog.test(login)) {
        errLogin.innerHTML = BAD_LOGIN;
        result = false;
    }

//password
    if (!password) {
        errPwd1.innerHTML = FILL_FIELD;
        result = false;
    }  // обязательное заполнение
    if (!confPassword) {
        errPwd2.innerHTML = FILL_FIELD;
        result = false;
    }  // обязательное заполнение

    var repsw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
    if (password && !repsw.test(password)) {
        errPwd1.innerHTML = BAD_PASSWORD;
        document.getElementById("password").value = "";   // сброс
        document.getElementById("passwordConfirm").value = "";   // сброс
        result = false;
    }

    if (password && confPassword && password !== confPassword) {
        errPwd1.innerHTML = PWD_NOT_EQUAL;
        errPwd2.innerHTML = PWD_NOT_EQUAL;
        document.getElementById("password").value = "";   // сброс
        document.getElementById("passwordConfirm").value = "";   // сброс
        result = false;
    }

//BD
    if (!bday) {
        errBday.innerHTML = FILL_FIELD;
        result = false;

    }
    if (bday) {
        var now = new Date();
        var ber = new Date(bday);
        var age = new Date(now - ber).getUTCFullYear() - 1970;
        if (age <= 0 || age > 120) {
            errBday.innerHTML = BAD_DATE;
            result = false;
        }
        else if (age < 18) {
            errBday.innerHTML = UNDER18;
            result = false;
        }
    }
    return result;
}

