const LOCALE = "ru";
const STRINGS = {
    error_empty_field: "* Поле не може быть пустым!",
    error_incorrect_login: "* Логин может содержать только латинские буквы, цыфры и знак подчеркивания",
    error_incorrect_password: "* Пароль должен иметь как минимум один символ в верхнем регистре, один в нижнем и одну цифру",
    error_different_passwords: "* Пароли не совпадают",
    error_incorrect_email: "* Не верный формат почты",
    error_incorrect_phone: "* Не верный формат телефона",
    error_under_18: "* Регистрация не возможна, Вам нет 18",
    error_incorrect_age:"* Не корректная дата"
};
Object.freeze(STRINGS);
