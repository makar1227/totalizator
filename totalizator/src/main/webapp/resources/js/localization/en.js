const LOCALE = "en";
const STRINGS = {
        error_empty_field: "* Field can not be empty!",
        error_incorrect_login: "* The login should contain only letters, numbers and the _ sign",
        error_incorrect_password: "* The password should contain minimum of 1 lower case letter ,1 upper case letter  and 1 numeric character",

        error_different_passwords: "* The Passwords are different",
        error_incorrect_email: "* Incorrect email format",
        error_incorrect_phone: "* Incorrect phone format",
        error_under_18: "* Sorry, you are under age",
        error_incorrect_age:"* Incorrect age"


    }
    ;
Object.freeze(STRINGS);
