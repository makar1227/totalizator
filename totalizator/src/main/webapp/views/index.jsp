<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>
<%@taglib prefix="om" uri="http://epam.mksn.by/tag/omTags" %>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>


<div class="col-lg-10 container ">
    <h1 class="page-header"><fmt:message key="main.header"/></h1>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-md-2" rowspan="2"><fmt:message key="main.table.date"/></th>
                <th class="col-md-2" rowspan="2"><fmt:message key="main.table.sport"/></th>
                <th class="col-md-3" rowspan="2"><fmt:message key="main.table.teamName"/></th>
                <th class="col-md-3" rowspan="2"><fmt:message key="main.table.teamName"/></th>
                <th class="col-md-3" colspan="3"><fmt:message key="main.table.coefficient"/></th>
                <th class="col-md-2" rowspan="2"><fmt:message key="main.table.bet"/></th>
            </tr>
            <tr>
                <th class="col-md-1"><fmt:message key="main.table.coefficient.firstWin"/></th>
                <th class="col-md-1"><fmt:message key="main.table.coefficient.draw"/></th>
                <th class="col-md-1"><fmt:message key="main.table.coefficient.secondWin"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="assumption" items="${assumptions}" varStatus="status" begin="${first}">
                    <td> <fmt:formatDate type="time" value="${assumption.match.date}" pattern="HH:mm dd.MM.yyyy"/></td>
                    <td><c:out value="${assumption.match.firstTeam.sport.sport}"/></td>
                    <td><c:out value="${assumption.match.firstTeam.name}"/></td>
                    <td><c:out value="${assumption.match.secondTeam.name}"/></td>
                    <td><c:out value="${assumption.fWin}"/></td>
                    <td><c:out value="${assumption.draw}"/></td>
                    <td><c:out value="${assumption.sWin}"/></td>
                    <td>
                        <c:if test="${authorizedUser.role!='BROKER'&& authorizedUser.role!='ADMIN'}">
                            <c:url value="/user/bet.html" var="betUrl"/>
                            <form action="${betUrl}" method="post">
                                <input type="hidden" name="id" value="${assumption.match.id}">
                                <button type="submit"><fmt:message key="main.table.button.user.bet"/></button>
                            </form>
                        </c:if>
                        <c:if test="${authorizedUser!=null && authorizedUser.role=='ADMIN'}">
                            <c:url value="/admin/match_edit.html" var="matchUrl"/>
                            <form action="${matchUrl}" method="post">
                                <input type="hidden" name="id" value="${assumption.match.id}">
                                <button type="submit"><fmt:message key="main.table.button.admin.result"/></button>
                            </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
        <c:url var="searchUri" value="/index.html?page=##"/>
        <om:display maxLinks="3" currPage="${page}" totalPages="${totalPages}" uri="${searchUri}"/>
</div>


<%@include file="/views/template/footer.jsp" %>