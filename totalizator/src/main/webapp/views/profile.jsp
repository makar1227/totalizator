<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>
<%@taglib prefix="om" uri="http://epam.mksn.by/tag/omTags" %>

<div class="col-lg-10 container ">
    <h1 class="page-header"><fmt:message key="user.header"/></h1>


    <div class="panel panel-info col-lg-5">
        <div class="panel-heading">
            <h3 class="panel-title">${user.login}</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <table class="table table-user-information">
                    <tbody>
                    <tr>
                        <td><fmt:message key="user.login"/></td>
                        <td>${user.login}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.phone"/></td>
                        <td>${user.phone}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.birth"/></td>
                        <td>${user.birth}</td>
                    </tr>

                    <tr>
                    <tr>
                        <td><fmt:message key="user.email"/></td>
                        <td>${user.email}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.cash"/></td>
                        <td>${user.cashAmount} $</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <c:if test="${not empty bets}">
    <div class="table-responsive col-lg-12 ">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-md-2" rowspan="2"><fmt:message key="user.table.date"/></th>
                <th class="col-md-1" rowspan="2"><fmt:message key="user.table.sport"/></th>
                <th class="col-md-2" rowspan="2"><fmt:message key="user.table.teamName"/></th>
                <th class="col-md-2" rowspan="2"><fmt:message key="user.table.teamName"/></th>
                <th class="col-md-3" colspan="2"><fmt:message key="user.table.bet"/></th>
                <th class="col-md-1" rowspan="2"><fmt:message key="user.table.stake"/></th>
                <th class="col-md-2" rowspan="2"><fmt:message key="user.table.edit"/></th>
            </tr>
            <tr>
                <th class="col-md-2"><fmt:message key="user.table.bet.commonResult"/></th>
                <th class="col-md-1"><fmt:message key="user.table.bet.score"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="bet" items="${bets}" varStatus="status" begin="${first}">
                <tr>
                    <td><fmt:formatDate type="time" value="${bet.match.date}" pattern="HH:mm dd.MM.yyyy"/></td>
                    <td><c:out value="${bet.match.firstTeam.sport.sport}"/></td>
                    <td><c:out value="${bet.match.firstTeam.name}"/></td>
                    <td><c:out value="${bet.match.secondTeam.name}"/></td>
                    <td><c:out value="${bet.result}"/></td>
                    <c:if test="${bet.firstTeamScore==-1}">
                        <td> -</td>
                    </c:if>
                    <c:if test="${bet.firstTeamScore!=-1}">
                        <td><c:out value="${bet.firstTeamScore}:${bet.secondTeamScore}"/></td>
                    </c:if>
                    <td><c:out value="${bet.cashSum}"/></td>
                    <td>
                        <jsp:useBean id="now" class="java.util.Date"/>
                        <c:if test="${bet.match.date > now}">
                            <c:url value="/user/bet_edit.html" var="betUrl"/>
                            <form action="${betUrl}" method="post">
                                <input type="hidden" name="id" value="${bet.id}">
                                <input type="hidden" name="matchId" value="${bet.match.id}">
                                <input type="hidden" name="firstTeamName" value="${bet.match.firstTeam.name}">
                                <input type="hidden" name="secondTeamName" value="${bet.match.secondTeam.name}">

                                <button type="submit"><fmt:message key="button.profile.bet.edit"/></button>
                            </form>
                            <form action="/user/bet_delete.html" method="post">
                                <input type="hidden" name="id" value="${bet.id}">
                                <button type="submit"><fmt:message key="button.profile.bet.delete"/></button>
                            </form>
                        </c:if>
                        <c:if test="${bet.match.date < now}">
                            <c:out value="${bet.match.fResult}:${bet.match.sResult}"/>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <c:url var="searchUri" value="/user/profile.html?page=##"/>
        <om:display maxLinks="3" currPage="${page}" totalPages="${totalPages}" uri="${searchUri}"/>
        </c:if>
    </div>
</div>
</div>

<%@include file="/views/template/footer.jsp" %>