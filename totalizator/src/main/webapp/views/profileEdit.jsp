<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-10 container">
    <h1 class="page-header"><fmt:message key="user.edit.header"/></h1>

    <div class="col-lg-9 contact_right">
        <form action="/user/profile/save/edit.html" method="post" onsubmit="return profileFormValidation(this);">

            <div class="form-group">
                <label><fmt:message key="user.edit.password"/>  <span class="error" id="err-password"/></label>
                <c:if test="${not empty error.attributes.passwordError}">
                    <span class="error"><fmt:message key="${error.attributes.passwordError}"/></span>
                </c:if>
                <input type="password" id="password" name="password" class="form-Control"/>
            </div>

            <div class="form-group">
                <label><fmt:message key="user.edit.newPassword"/>  <span class="error" id="err-newPassword"/></label>
                <c:if test="${not empty error.attributes.simplePasswordError}">
                    <span class="error"><fmt:message key="${error.attributes.simplePasswordError}"/></span>
                </c:if>
                <input type="password" id="newPassword" name="newPassword" class="form-Control"/>
            </div>

            <div class="form-group">
                <label for="email"><fmt:message key="user.edit.email"/>  <span class="error" id="err-email"/></label>
                <c:if test="${not empty error.attributes.emailError}">
                    <span class="error"><fmt:message key="${error.attributes.emailError}"/></span>
                </c:if>
                <input type="email" id="email" name="email" class="form-Control" value="${user.email}"/>
            </div>

            <div class="form-group">
                <label for="phone"><fmt:message key="user.edit.phone"/> <span class="error" id="err-phone"/></label>
                <c:if test="${not empty error.attributes.phoneError}">
                    <span class="error"><fmt:message key="${error.attributes.phoneError}"/></span>
                </c:if>
                <input type="text" id="phone" name="phone" class="form-Control" value="${user.phone}"/>
            </div>


            <div class="form-group">
                <label for="cash"><fmt:message key="user.edit.cash"/> </label>
                <c:if test="${not empty error.attributes.cashError}">
                    <span class="error"><fmt:message key="${error.attributes.cashError}"/></span>
                </c:if>
                <input type="number" id="cash" name="cash" class="form-Control" min="100" max="100000" step="0.01"
                       value="${user.cashAmount}"/>
            </div>

            <br>
            <br>
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="user/profile.html" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </form>
    </div>
</div>
</div>
<script src="<c:url value="/resources/js/profileEdit.js"/>"></script>
<%@include file="/views/template/footer.jsp" %>