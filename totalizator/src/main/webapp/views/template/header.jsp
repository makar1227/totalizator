<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Totalizator</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel='stylesheet' type='text/css'/>
    <link href="<c:url value="/resources/css/main.css"/>" rel='stylesheet' type='text/css'/>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,700italic,800italic,400,300,700,800,600'
          rel='stylesheet' type='text/css'>

    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet" type="text/css" media="all"/>

    <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/localization/${sessionScope.locale}.js"/>"></script>

</head>
<body>
<div class="header_bg" id="home"><!-- start header -->
    <div class="container">
        <div class="header">
            <div class="logo navbar-left">
                <h1><img src="<c:url value="/resources/images/logo.png"/>" alt=""
                         class="img-responsive"/></h1>
            </div>
            <div class="h_menu navbar-right">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href='<c:url value="/" />'><fmt:message key="menu.home"/> </a></li>
                            <c:if test="${authorizedUser!=null}">
                                <li>
                                    <a><fmt:message key="menu.welcome"/>: ${authorizedUser.login}</a>
                                </li>
                                <li><a href='<c:url value="/logout.html" />'><fmt:message key="menu.logout"/> </a></li>
                            </c:if>
                            <c:if test="${authorizedUser==null}">
                                <li><a href='<c:url value="/registration.html" />'><fmt:message
                                        key="menu.registration"/> </a></li>
                                <li><a href='<c:url value="/login.html" />'><fmt:message key="menu.login"/> </a></li>
                            </c:if>
                            <li><a href="<c:url value="/lang.html?cmd=set_locale&locale=ru"/> ">Ru</a></li>
                            <li><a href="<c:url value="/lang.html?cmd=set_locale&locale=en"/> ">Eng</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>

            </div>
        </div>
    </div>
</div><!-- end header -->

