<div class="container-fluid footer_bg">
    <div class="row">
        <div class="col-lg-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#"><span class="sr-only">(current)</span></a></li>
                <c:forEach items="${menu}" var="item">
                    <li><a href="${item}"><fmt:message key="${item}"/></a></li>
                </c:forEach>

            </ul>
        </div>
