<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-10 container">
    <h1 class="page-header"><fmt:message key="registration.header"/>!</h1>

    <div class="col-lg-9 contact_right">
        <form id="registrationForm"
              action="/registration/validation.html" method="post"
              onsubmit="return registrationFormValidation(this);">

            <div class="form-group ">
                <label for="login"><fmt:message key="registration.form.login"/>: <span class="error"
                                                                                       id="err-login"/></label>
                <c:if test="${not empty error.attributes.loginError}">
                    <span class="error"><fmt:message key="${error.attributes.loginError}"/></span>
                </c:if>
                <input type="text" id="login" name="login" class="form-Control " value="${user.login}"/>

            </div>
            <div class="form-group">
                <label><fmt:message key="registration.form.password"/>: <span class="error" id="err-password"/></label>

                <c:if test="${not empty error.attributes.passwordError}">
                    <span class="error"><fmt:message key="${error.attributes.passwordError}"/></span>
                </c:if>
                <c:if test="${not empty error.attributes.simplePasswordError}">
                    <span class="error"><fmt:message key="${error.attributes.simplePasswordError}"/></span>
                </c:if>
                <input type="password" id="password" name="password" class="form-Control"
                       value="${user.password}"/>
            </div>

            <div class="form-group">
                <label><fmt:message key="registration.form.repeatPasword"/>: <span class="error"
                                                                                   id="err-passwordConfirm"/> </label>
                <input type="password" id="passwordConfirm" name="passwordConfirm" class="form-Control"
                       value="${user.password}"/>
            </div>

            <div class="form-group">
                <label for="email"><fmt:message key="registration.form.email"/>: <span class="error"
                                                                                       id="err-email"/></label>
                <c:if test="${not empty error.attributes.emailError}">
                    <span class="error"><fmt:message key="${error.attributes.emailError}"/></span>
                </c:if>

                <input type="email" id="email" name="email" class="form-Control"
                       placeholder="ivan.doe@example.com"
                       value="${user.email}"/>


            </div>

            <div class="form-group">
                <label for="phone"><fmt:message key="registration.form.phone"/>: <span class="error"
                                                                                       id="err-phone"/></label>
                <c:if test="${not empty error.attributes.phoneError}">
                    <span class="error"><fmt:message key="${error.attributes.phoneError}"/></span>
                </c:if>

                <input type="text" id="phone" name="phone" class="form-Control col-lg-4" placeholder="+37529-***-**-**"
                       value="${user.phone}"/>

            </div>


            <div class="form-group">
                <label for="birth"><fmt:message key="registration.form.birth"/>: <span class="error"
                                                                                       id="err-bday"/></label>
                <c:if test="${not empty error.attributes.birthError}">
                    <span class="error"><fmt:message key="${error.attributes.birthError}"/></span>
                </c:if>
                <input type="date" id="birth" name="birth" class="form-Control col-lg-4" min="1900-01-01"
                       value="${user.birth}"/>
            </div>

            <div class="form-group">
                <label for="cash"><fmt:message key="registration.form.cash"/>: </label>
                <c:if test="${not empty error.attributes.cashError}">
                    <span class="error"><fmt:message key="${error.attributes.cashError}"/></span>
                </c:if>
                <input type="number" id="cash" name="cash" class="form-Control col-lg-4" min="100" max="100000"
                       step="0.01"
                       value="${user.cashAmount}"/>
            </div>

            <br>
            <br>
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </form>

    </div>
</div>
</div>
<script src="<c:url value="/resources/js/registration.js"/>"></script>
<%@include file="/views/template/footer.jsp" %>
