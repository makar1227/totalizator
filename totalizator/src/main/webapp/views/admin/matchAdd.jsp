<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-8 container ">
    <h1 class="page-header"><fmt:message key="admin.add.header"/></h1>

    <div class="col-lg-5 contact_right">
        <form action="/admin/match_add/success.html" method="post">

            <div class="form-group ">
                <label><fmt:message key="admin.add.date"/> </label>
                <c:if test="${not empty error.attributes.dateError}">
                    <span class="error"><fmt:message key="${error.attributes.dateError}"/></span>
                </c:if>

                <input type="datetime-local" id="date" name="date" class="form-Control" placeholder="2017-02-31 22:30"/>
            </div>

            <div class="form-group">
                <label><fmt:message key="admin.add.firstTeam"/></label>
                <c:if test="${not empty error.attributes.sameTeamError}">
                    <span class="error"><fmt:message key="${error.attributes.sameTeamError}"/></span>
                </c:if>

                <select name="fTeam" class="form-Control">
                    <c:forEach var="team" items="${list}">
                        <option value="${team.name}">
                            <c:out value="${team.name} (${team.sport.sport})"/>
                        </option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label><fmt:message key="admin.add.secondTeam"/></label>
                <select name="sTeam" class="form-Control">
                    <c:forEach var="team" items="${list}">
                        <option value="${team.name}">
                            <c:out value="${team.name} (${team.sport.sport})"/>
                        </option>
                    </c:forEach>
                </select>
            </div>

            <br>
            <br>
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </form>

    </div>

</div>
</div>

<%@include file="/views/template/footer.jsp" %>
