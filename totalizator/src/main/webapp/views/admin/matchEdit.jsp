<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-10 container ">
    <h1 class="page-header"><fmt:message key="admin.edit.header"/> </h1>
    <div class="col-lg-4 contact_right">
        <form action="/admin/match_edit/success.html" method="post">

            <div class="form-group">
                <label>${match.firstTeam.name} <fmt:message key="admin.edit.score"/></label>
                <input type="number" name="fScore" class="form-Control" min="0"/>
            </div>

            <div class="form-group">
                <label>${match.secondTeam.name} <fmt:message key="admin.edit.score"/></label>
                <input type="number" name="sScore" class="form-Control" min="0"/>
            </div>

            <br>
            <br>
            <input type="hidden" name="id" value="${match.id}">
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </form>
    </div>

</div>
</div>


<%@include file="/views/template/footer.jsp" %>