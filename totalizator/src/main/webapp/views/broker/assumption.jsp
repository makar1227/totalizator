<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>
<%@taglib prefix="om" uri="http://epam.mksn.by/tag/omTags" %>

<div class="col-lg-10 container">
    <h1 class="page-header"><fmt:message key="broker.header"/></h1>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th><fmt:message key="broker.table.date"/></th>
                <th><fmt:message key="broker.table.sport"/></th>
                <th><fmt:message key="broker.table.team"/></th>
                <th><fmt:message key="broker.table.team"/></th>

                <th rowspan="2"><fmt:message key="broker.table.assumption"/></th>
            </tr>

            </thead>
            <tbody>
            <c:forEach var="match" items="${matches}" varStatus="status" begin="${first}">
                <tr>
                    <td> <fmt:formatDate type="time" value="${match.date}" pattern="HH:mm dd.MM.yyyy"/></td>
                    <td><c:out value="${match.firstTeam.sport.sport}"/></td>
                    <td><c:out value="${match.firstTeam.name}"/></td>
                    <td><c:out value="${match.secondTeam.name}"/></td>
                    <td>

                        <c:url value="/broker/assumption_add.html" var="assumUrl"/>
                        <form action="${assumUrl}" method="post">
                            <input type="hidden" name="match" value="${match.id}">
                            <input type="hidden" name="fTeam" value="${match.firstTeam.name}">
                            <input type="hidden" name="sTeam" value="${match.secondTeam.name}">
                            <button type="submit"><fmt:message key="broker.table.button"/></button>
                        </form>

                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
    <c:url var="searchUri" value="/broker/assumption.html?page=##"/>
    <om:display maxLinks="3" currPage="${page}" totalPages="${totalPages}" uri="${searchUri}"/>

</div>

<%@include file="/views/template/footer.jsp" %>