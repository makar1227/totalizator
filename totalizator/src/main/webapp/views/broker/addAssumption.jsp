<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-9 container contact_right">
    <h1 class="page-header"><fmt:message key="broker.assumption.header"/></h1>

    <div class="col-lg-4 ">
        <c:url value="/broker/assumption_save.html" var="assumptionUrl"/>
        <form action="${assumptionUrl}" method="post">
            <input type="hidden" name="match" value="${match}">


            <div class="form-group ">
                <label for="fScore"><fmt:message key="broker.assumption.win"/> ${fTeam} </label>
                <input type="number" id="fScore" name="fScore" class="form-Control" min="1" max="100" step="0.01"/>
            </div>
            <div class="form-group">
                <label for="sScore"> <fmt:message key="broker.assumption.draw"/> </label>
                <input type="number" id="draw" name="draw" class="form-Control" min="1" max="100" step="0.01"/>
            </div>

            <div class="form-group">
                <label for="sScore"><fmt:message key="broker.assumption.win"/> ${sTeam} </label>
                <input type="number" id="sScore" name="sScore" class="form-Control" min="1" max="100" step="0.01"/>
            </div>

            <br> <br>
            <input type="submit" value="<fmt:message key="button.submit"/> " class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/> </a>

        </form>
    </div>
</div>
</div>


<%@include file="/views/template/footer.jsp" %>