<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-9 container contact_right">
    <h1 class="page-header"><fmt:message key="user.bet.edit.header"/></h1>

    <form action="/user/bet_edit/success.html" method="post">

        <input type="hidden" name="betId" value="${bet.id}">
        <input type="hidden" name="matchId" value="${bet.match.id}">
        <input type="hidden" name="firstTeamName" value="${firstTeamName}">
        <input type="hidden" name="secondTeamName" value="${secondTeamName}">

        <div class="form-group">
            <label><fmt:message key="user.bet.edit.commonResult"/> </label>
            <select name="result" class="form-Control" selected="${bet.result}">
                <option value="1" ${bet.result=='FIRST_WIN' ?'selected="selected"' : ''} ><c:out
                        value="${firstTeamName} "/> <fmt:message key="user.bet.edit.win"/></option>
                <option value="x" ${bet.result=='DRAW' ?'selected="selected"' : ''} ><fmt:message
                        key="user.bet.edit.draw"/></option>
                <option value="2" ${bet.result=='SECOND_WIN' ?'selected="selected"' : ''} ><c:out
                        value="${secondTeamName} "/> <fmt:message key="user.bet.edit.win"/></option>
            </select>
        </div>

        <h3 class="page-header"><fmt:message key="user.bet.edit.exactResult"/></h3>
        <div class="form-group">
            <label>${firstTeamName}</label>
            <c:if test="${not empty error.attributes.fScoreError}">
                <span class="error"><fmt:message key="${error.attributes.fScoreError}"/></span>
            </c:if>
            <c:if test="${not empty error.attributes.scoreError}">
                <span class="error"><fmt:message key="${error.attributes.scoreError}"/></span>
            </c:if>
            <input type="number" name="fScore" class="form-Control" min="0" step="1"
                   value="${bet.firstTeamScore == -1 ? '' :bet.firstTeamScore}"/>
        </div>

        <div class="form-group">
            <label>${secondTeamName}</label>
            <c:if test="${not empty error.attributes.sScoreError}">
                <span class="error"><fmt:message key="${error.attributes.sScoreError}"/></span>
            </c:if>
            <input type="number" name="sScore" class="form-Control" min="0" step="1"
                   value="${bet.secondTeamScore == -1 ? '':bet.secondTeamScore}"/>
        </div>

        <div class="form-group">
            <label for="cash"><fmt:message key="user.bet.edit.wager"/></label>
            <c:if test="${not empty error.attributes.cashError}">
                <span class="error"><fmt:message key="${error.attributes.cashError}"/></span>
            </c:if>
            <c:if test="${not empty error.attributes.overCashError}">
                <span class="error"><fmt:message key="${error.attributes.overCashError}"/></span>
            </c:if>
            <input type="number" id="cash" name="cash" class="form-Control" value="${bet.cashSum}" min="10"
                   step="0.01"/>
        </div>
        <br>
        <br>
        <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
        <a href="<c:url value="/user/profile.html" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
    </form>

</div>
</div>

<%@include file="/views/template/footer.jsp" %>
