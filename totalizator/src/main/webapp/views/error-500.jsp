<%@include file="/views/template/header.jsp"%>
<%@include file="/views/template/menu.jsp"%>

<div class="col-lg-10 container ">
    <h1 class="page-header"><fmt:message key="error.500.title"/></h1>
    <h2><fmt:message key="error.500.message"/></h2>
</div>
</div>


<%@include file="/views/template/footer.jsp" %>