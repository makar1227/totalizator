<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-10 container contact_right">
    <h1 class="page-header"><fmt:message key="login.header"/></h1>

    <div class="col-lg-4">
        <c:url value="/authorised.html" var="loginUrl"/>
        <form action="${loginUrl}" method="post">
            <div class="form-group ">
                <label for="login"><fmt:message key="login.login"/></label>
                <c:if test="${not empty error.attributes.loginError}">
                    <span class="error"><fmt:message key="${error.attributes.loginError}"/></span>
                </c:if>
                <input type="text" id="login" name="login" value="${user.login}" maxlength="30"/>
            </div>
            <div class="form-group ">
                <label for="password"><fmt:message key="login.password"/></label>
                <c:if test="${not empty error.attributes.passwordError}">
                    <span class="error"><fmt:message key="${error.attributes.passwordError}"/></span>
                </c:if>
                <input type="password" id="password" name="password" value="${param.password}" maxlength="30"/>
            </div>
            <br> <br>
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </form>

        <c:url value="/registration.html" var="registrationUrl"/>
        <form action="${registrationUrl}" method="post">
            <br>
            <input type="submit" value="<fmt:message key="login.button.registration"/>"/>

        </form>
    </div>
</div>
</div>

<%@include file="/views/template/footer.jsp" %>