<%@include file="/views/template/header.jsp" %>
<%@include file="/views/template/menu.jsp" %>

<div class="col-lg-9 container contact_right">
    <h1 class="page-header"><fmt:message key="user.bet.add.header"/></h1>
    <c:url value="/user/bet/register.html" var="betUrl"/>
    <form action="${betUrl}" method="post">
        <input type="hidden" name="matchId" value="${match.id}">
        <div class="form-group">
            <lable for="condition"><b><fmt:message key="user.bet.add.commonResult"/>:</b></lable>
            <label class="radio">
                <input type="radio" name="result" ${bet.result=='FIRST_WIN' ?'checked="true"' : ''} id="result1"
                       value="1"/>
                <fmt:message key="user.bet.add.win"/> ${match.firstTeam.name} </label>
            <label class="radio">
                <input type="radio" name="result"  ${bet.result=='DRAW' ?'checked="true"' : ''} id="resultx" value="x"/>
                <fmt:message key="user.bet.add.draw"/> </label>
            <label class="radio">
                <input type="radio" name="result"  ${bet.result=='SECOND_WIN' ?'checked="true"' : ''} id="result2"
                       value="2"/>
                <fmt:message key="user.bet.add.win"/> ${match.secondTeam.name}</label>
        </div>


        <div class="col-lg-5">
            <h3 class="page-header"><fmt:message key="user.bet.add.exactResult"/></h3>
            <div class="form-group ">
                <label for="fScore">${match.firstTeam.name} <fmt:message key="user.bet.add.score"/></label>
                <c:if test="${not empty error.attributes.fScoreError}">
                    <span class="error"><fmt:message key="${error.attributes.fScoreError}"/></span>
                </c:if>
                <c:if test="${not empty error.attributes.scoreError}">
                    <span class="error"><fmt:message key="${error.attributes.scoreError}"/></span>
                </c:if>

                <input type="number" id="fScore" name="fScore"
                       value="${bet.firstTeamScore == -1 ? '' :bet.firstTeamScore}" class="form-Control" min="0"
                       max="100" step="1"/>
            </div>
            <div class="form-group ">
                <label for="sScore"> ${match.secondTeam.name} <fmt:message key="user.bet.add.score"/></label>
                <c:if test="${not empty error.attributes.sScoreError}">
                    <span class="error"><fmt:message key="${error.attributes.sScoreError}"/></span>
                </c:if>
                <input type="number" id="sScore" name="sScore"
                       value="${bet.secondTeamScore == -1 ? '':bet.secondTeamScore}" class="form-Control" min="0"
                       max="100" step="1"/>
            </div>

            <div class="form-group ">
                <label for="cash"><fmt:message key="user.bet.add.cash"/></label>
                <c:if test="${not empty error.attributes.cashError}">
                    <span class="error"><fmt:message key="${error.attributes.cashError}"/></span>
                </c:if>
                <c:if test="${not empty error.attributes.overCashError}">
                    <span class="error"><fmt:message key="${error.attributes.overCashError}"/></span>
                </c:if>
                <input type="number" id="cash" name="cash" value="${bet.cashSum}" class="form-Control" min="10"
                       max="100000" step="0.1"/>
            </div>

            <br> <br>
            <input type="submit" value="<fmt:message key="button.submit"/>" class="btn btn-default">
            <a href="<c:url value="/" />" class="btn btn-default"><fmt:message key="button.cancel"/></a>
        </div>
    </form>
</div>
</div>

<%@include file="/views/template/footer.jsp" %>