package by.epam.makarevich.totalizator.controller;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.dao.impl.MySqlDaoFactory;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.service.impl.ServiceFactoryImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Dispatcher servlet.
 */
public class DispatcherServlet extends HttpServlet {
    private static final Logger logger = Logger.getRootLogger();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug(req.getAttribute("action"));
        Command.Forward commandForward = null;
        Command command = (Command) req.getAttribute("action");
        try {
//            TODO delete from this layer
            command.setFactory(new ServiceFactoryImpl(MySqlDaoFactory.getMyFactory()));
            commandForward = command.exec(req, resp);

            if (commandForward != null && commandForward.isRedirect()) {
                String redirectedUri = req.getContextPath() + commandForward.getForward();
                resp.sendRedirect(redirectedUri);
            } else if (commandForward != null) {
                req.getRequestDispatcher("/views/" + commandForward.getForward()).forward(req, resp);
            }
        } catch (TotalizatorSqlException | TotalizatorServiceException e) {
            logger.error("It is a problem with processing request", e);
            getServletContext().getRequestDispatcher("/views/error.jsp");
        }
    }

    @Override
    public void destroy() {
        MyConnectionPool.getInstance().dispose();
    }
}
