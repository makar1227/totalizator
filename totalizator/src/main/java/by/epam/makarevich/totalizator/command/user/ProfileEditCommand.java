package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.interfaces.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ProfileEditCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        try {
            UserService userService = factory.getService(UserService.class);
            User user = userService.getUserByLogin(((Authorities) session.getAttribute("authorizedUser")).getLogin());
            req.setAttribute("user", user);
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("/error.jsp");
        }
        return new Forward("/profileEdit.jsp");
    }
}
