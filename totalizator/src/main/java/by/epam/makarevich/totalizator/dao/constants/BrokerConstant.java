package by.epam.makarevich.totalizator.dao.constants;

public class BrokerConstant {
    public static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
}
