package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.model.Authorities;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LogOutCommand extends Command {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Authorities authorities =(Authorities) req.getSession().getAttribute("authorizedUser");
        logger.info(String.format("user \"%s\" is logged out", authorities.getLogin()));
        req.getSession(false).invalidate();
        return new Forward("/", true);
    }
}
