package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Bet;

import java.util.List;

public interface BetService extends Service {

    Bet getBetById(int id) throws TotalizatorServiceException;

    List<Bet> getMatchBets(int matchId) throws TotalizatorServiceException;

    int getCountUserBets(int userId) throws TotalizatorServiceException;

    List<Bet> getUserBets(int userId, int start, int count) throws TotalizatorServiceException;

    boolean addBet(Bet bet) throws TotalizatorServiceException;

    void deleteBet(int id) throws TotalizatorServiceException;

    void editBet(Bet bet) throws TotalizatorServiceException;

}
