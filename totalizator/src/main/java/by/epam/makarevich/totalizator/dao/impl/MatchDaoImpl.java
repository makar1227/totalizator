package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.MatchConstant;
import by.epam.makarevich.totalizator.dao.interfaces.MatchDao;
import by.epam.makarevich.totalizator.dao.interfaces.UserDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.*;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

import java.util.List;


/**
 * The type Match dao.
 */

public class MatchDaoImpl implements MatchDao {

    private static Logger logger = Logger.getLogger(Match.class);

    private static final String QUERY_INSERT = "INSERT INTO  totalizator.match (first_team_id, second_team_id, match_date) VALUES (?,?,?)";
    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM  totalizator.match WHERE id=?";
    private static final String QUERY_UPDATE_MATCH_STATUS = "UPDATE  totalizator.match SET match_status=? WHERE id=?";
    private static final String QUERY_SELECT_VALID_MATCHES = "SELECT  M.id, M.match_date, T1.sport, T1.team_name T1Name, T2.team_name T2Name FROM totalizator.team T1 " +
            "INNER join totalizator.match M ON M.first_team_id=T1.id INNER JOIN totalizator.team T2 " +
            "ON M.second_team_id=T2.id WHERE M.match_status=? AND M.match_date > CURRENT_TIMESTAMP ORDER BY M.match_date LIMIT ?, ?;";
    private static final String QUERY_COUNT_VALID_MATCHES = "SELECT COUNT(*) FROM totalizator.team T1 " +
            "INNER join totalizator.match M ON M.first_team_id=T1.id INNER JOIN totalizator.team T2 " +
            "ON M.second_team_id=T2.id WHERE M.match_status=? AND M.match_date > CURRENT_TIMESTAMP;";
    private static final String QUERY_UPDATE = "UPDATE  totalizator.match SET result_common=?, " +
            "result_first_team=?, result_second_team=?, match_status=? WHERE id=?";


    private List<Match> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Match> matches = new ArrayList<>();
        try {
            while (result.next()) {
                Match match = new Match();
                match.setId(result.getInt(MatchConstant.ID));
                Team team1 = new Team();
                team1.setId(result.getInt(MatchConstant.FIRST_TEAM_ID));
                match.setFirstTeam(team1);
                Team team2 = new Team();
                team2.setId(result.getInt(MatchConstant.SECOND_TEAM_ID));
                match.setSecondTeam(team2);
                match.setDate(result.getTimestamp(MatchConstant.DATE));
                if (result.getString(MatchConstant.RESULT) != null) {
                    Result matchResult = Result.getByName(result.getString(MatchConstant.RESULT));
                    match.setResult(matchResult);
                    match.setfResult(result.getInt(MatchConstant.FIRST_TEAM_SCORE));
                    match.setsResult(result.getInt(MatchConstant.SECOND_TEAM_SCORE));
                }
                match.setMatchStatus(result.getBoolean(MatchConstant.MATCH_STATUS));
                matches.add(match);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Match resultSet.", e);
        }
        return matches;
    }

    private void prepareStatementForInsert(PreparedStatement statement, Match match) throws TotalizatorSqlException {

        try {
            statement.setInt(1, match.getFirstTeam().getId());
            statement.setInt(2, match.getSecondTeam().getId());

            java.sql.Timestamp newDate = new java.sql.Timestamp(match.getDate().getTime());
            statement.setTimestamp(3, newDate);

        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for insert Match.", e);
        }
    }


    private void prepareStatementForUpdate(PreparedStatement statement, Match match) throws TotalizatorSqlException {
        try {
            statement.setInt(1, match.getResult().getId(match.getResult()));
            statement.setInt(2, match.getfResult());
            statement.setInt(3, match.getsResult());
            statement.setBoolean(4, match.isMatchStatus());
            statement.setInt(5, match.getId());

        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for update Match.", e);
        }
    }

    @Override
    public List<Match> getMatchesByStatus(boolean valid, int start, int count) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_VALID_MATCHES;
        List<Match> matches = new ArrayList<>();
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, valid);
            statement.setInt(2, start);
            statement.setInt(3, count);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Match match = new Match();
                    Team team1 = new Team();
                    Team team2 = new Team();
                    match.setId(result.getInt(MatchConstant.ID));
                    match.setDate(result.getTimestamp(MatchConstant.DATE));
                    team1.setName(result.getString(MatchConstant.FIRST_TEAM_NAME));
                    Sport sport = Sport.getByName(result.getString(MatchConstant.SPORT));
                    team1.setSport(sport);
                    team2.setName(result.getString(MatchConstant.SECOND_TEAM_NAME));
                    match.setFirstTeam(team1);
                    match.setSecondTeam(team2);

                    matches.add(match);
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Match by status.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return matches;
    }

    @Override
    public int getCountMatchesByStatus(boolean valid) throws TotalizatorSqlException {
        String sql = QUERY_COUNT_VALID_MATCHES;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        int count = 0;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, valid);
            try (ResultSet result = statement.executeQuery()) {
                result.next();
                count = result.getInt(1);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with counting Matches.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return count;
    }

    @Override
    public void editMatchStatus(Match match) throws TotalizatorSqlException {
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try {
            editMatchStatus(connection, match);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    @Override
    public void editMatchStatus(Connection connection, Match match) throws TotalizatorSqlException {
        String sql = QUERY_UPDATE_MATCH_STATUS;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, match.isMatchStatus());
            statement.setInt(2, match.getId());
            int count = statement.executeUpdate();
            if (count == 1) {
                logger.info("Match with id = " + match.getId() + "was successfully updated!");
            } else {
                throw new TotalizatorSqlException("Match with id = " + match.getId() + "wasn't updated!");
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Can not update Match status with id = " + match.getId());
        }
    }

    @Override
    public Match getMatchById(int id) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_ID;
        List<Match> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting match by id ", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public boolean addMatch(Match match) throws TotalizatorSqlException {
        String sql = QUERY_INSERT;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, match);
            int count = statement.executeUpdate();
            if (count == 1) {
                logger.info("Match was successfully created!");
            } else {
                logger.error("Match wasn't added ");
                throw new TotalizatorSqlException("Match wasn't added ");
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Cannot insert match object in DB", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return true;
    }

    @Override
    public void editMatch(Match match, Assumption assumption, List<Bet> bets) throws TotalizatorSqlException {
        String sql = QUERY_UPDATE;

        UserDao userDao = new UserDaoImpl();
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(statement, match);
            User admin = userDao.getUserByLogin(MatchConstant.ADMIN);

            connection.setAutoCommit(false);
            for (Bet bet : bets) {
                User user = userDao.getUserById(bet.getUser().getId());
                this.cashCount(admin, user, bet, match, assumption);

                userDao.editUserCash(connection, user);
                userDao.editUserCash(connection, admin);
            }
            int count = statement.executeUpdate();

            connection.commit();

            if (count == 1) {
                logger.info("Match with id = " + match.getId() + " was successfully updated!");
            } else {
                throw new TotalizatorSqlException("Match with id = " + match.getId() + " wasn't updated.");
            }

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new TotalizatorSqlException("Can not update match object in DB, rollback operation failed", e);
            }
            throw new TotalizatorSqlException("Can not update match with id = " + match.getId());
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    private void cashCount(User admin, User user, Bet bet, Match match, Assumption assumption) {
        double cash = 0;
        if (bet.getResult() == match.getResult()) {
            switch (bet.getResult()) {
                case FIRST_WIN:
                    cash = bet.getCashSum() * assumption.getfWin();
                    break;
                case DRAW:
                    cash = bet.getCashSum() * assumption.getDraw();
                    break;
                case SECOND_WIN:
                    cash = bet.getCashSum() * assumption.getsWin();
                    break;
            }
            if (bet.getFirstTeamScore() == match.getfResult() && bet.getSecondTeamScore() == match.getsResult()) {
                cash = cash * MatchConstant.WIN_COEFFICIENT;
            }

            user.setCashAmount(user.getCashAmount() + cash);
            admin.setCashAmount(admin.getCashAmount() - (cash - bet.getCashSum()));
        } else {
            admin.setCashAmount(admin.getCashAmount() + bet.getCashSum());
        }
    }

    public MatchDaoImpl() {
        super();
    }
}
