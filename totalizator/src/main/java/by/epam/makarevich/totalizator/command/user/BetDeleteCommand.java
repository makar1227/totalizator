package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.service.interfaces.BetService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BetDeleteCommand extends Command {

    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        try{
            BetService betService = factory.getService(BetService.class);
            betService.deleteBet(Integer.parseInt(req.getParameter("id")));
            return new Forward("/user/profile.html",true);
        }
        catch (TotalizatorServiceException e){
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }

    }
}
