package by.epam.makarevich.totalizator.model;


/**
 * The enum Result.
 */
public enum Result {
    /**
     * First win result.
     */
    FIRST_WIN("1"),
    /**
     * Draw result.
     */
    DRAW("x"),
    /**
     * Second win result.
     */
    SECOND_WIN("2");
    private String result;

    private Result(String result) {
        this.result = result;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * Gets id.
     *
     * @param result the result
     * @return the id
     */
    public static int getId(Result result) {
        switch (result) {
            case FIRST_WIN:
                return 1;
            case DRAW:
                return 2 ;
            case SECOND_WIN:
                return 3;
            default:
                return 0;
        }
    }

    /**
     * Gets by name.
     *
     * @param name the name
     * @return the by name
     */
    public static Result getByName(String name) {

        switch (name) {
            case "1":
                return FIRST_WIN;
            case "x":
                return DRAW;
            case "2":
                return SECOND_WIN;
            default:
                return null;
        }
    }
}
