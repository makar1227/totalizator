package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Team;
import by.epam.makarevich.totalizator.service.interfaces.TeamService;

import java.util.List;


/**
 * The type Team service.
 */
public class TeamServiceImpl extends ServiceImpl implements TeamService {
    @Override
    public Team getTeamById(int id) throws TotalizatorServiceException {
        try {
            return factory.getTeamDao().getTeamById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public Team getTeamByName(String name) throws TotalizatorServiceException {
        try {
            return factory.getTeamDao().getTeamByName(name);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public List<Team> getAllTeams() throws TotalizatorServiceException {
        try {
            return factory.getTeamDao().getAllTeams();
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
