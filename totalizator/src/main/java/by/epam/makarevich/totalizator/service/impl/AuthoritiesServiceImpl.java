package by.epam.makarevich.totalizator.service.impl;


import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.service.interfaces.AuthoritiesService;


/**
 * The type Authorities service.
 */
public class AuthoritiesServiceImpl extends ServiceImpl implements AuthoritiesService {
    @Override
    public Authorities getAuthoritiesById(int id) throws TotalizatorServiceException {
        try {
            return factory.getAuthoritiesDao().getAuthorityById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public Authorities getAuthoritiesByLogin(String login) throws TotalizatorServiceException {
        try {
            return factory.getAuthoritiesDao().getAuthorityByLogin(login);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public boolean addAuthority(Authorities authorities) throws TotalizatorServiceException {
        try {
            return factory.getAuthoritiesDao().addAuthority(authorities);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public void editAuthority(Authorities authorities) throws TotalizatorServiceException {
        try {
            factory.getAuthoritiesDao().editAuthority(authorities);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
