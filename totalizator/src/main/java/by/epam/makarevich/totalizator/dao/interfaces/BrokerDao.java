package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Broker;

/**
 * The interface Broker dao.
 */
public interface BrokerDao {

    Broker getBrokerByLogin(String login) throws TotalizatorSqlException;

    Broker getBrokerById(int id) throws TotalizatorSqlException;

}
