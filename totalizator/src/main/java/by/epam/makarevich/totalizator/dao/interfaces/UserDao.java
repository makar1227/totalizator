package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.User;

import java.sql.Connection;

public interface UserDao {

    User getUserByEmail(String email) throws TotalizatorSqlException;

    User getUserByLogin(String login) throws TotalizatorSqlException;

    User getUserById(int id) throws TotalizatorSqlException;

    boolean addUser(User user) throws TotalizatorSqlException;

    void editUser(User user) throws TotalizatorSqlException;

    void editUserCash(Connection connection, User user) throws TotalizatorSqlException;

}

