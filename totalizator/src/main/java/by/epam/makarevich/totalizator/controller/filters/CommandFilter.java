package by.epam.makarevich.totalizator.controller.filters;

import by.epam.makarevich.totalizator.command.user.*;
import by.epam.makarevich.totalizator.command.*;
import by.epam.makarevich.totalizator.command.admin.MatchAddCommand;
import by.epam.makarevich.totalizator.command.admin.MatchAddPageCommand;
import by.epam.makarevich.totalizator.command.admin.MatchEditCommand;
import by.epam.makarevich.totalizator.command.admin.MatchEditPageCommand;
import by.epam.makarevich.totalizator.command.broker.AssumptionAddPageCommand;
import by.epam.makarevich.totalizator.command.broker.AssumptionCommand;
import by.epam.makarevich.totalizator.command.broker.AssumptionPageCommand;
import by.epam.makarevich.totalizator.command.pages.LoginPageCommand;
import by.epam.makarevich.totalizator.command.pages.ProfilePageCommand;
import by.epam.makarevich.totalizator.command.pages.RegistrationPageCommand;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The type Command filter.
 */
@WebFilter(urlPatterns = {"*.html"})
public class CommandFilter implements Filter {

    protected static Logger logger = Logger.getRootLogger();
    private static Map<String, Class<? extends Command>> commands = new ConcurrentHashMap<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        commands.put("/", MainCommand.class);
        commands.put("/index.html", MainCommand.class);
        commands.put("/login.html", LoginPageCommand.class);
        commands.put("/authorised.html", LoginCommand.class);
        commands.put("/logout.html", LogOutCommand.class);
        commands.put("/lang.html", LocaleCommand.class);

        commands.put("/registration.html", RegistrationPageCommand.class);
        commands.put("/registration/validation.html", RegistrationCommand.class);

        commands.put("/user/bet.html", BetCommand.class);
        commands.put("/user/bet/register.html", BetSaveCommand.class);
        commands.put("/user/bet_edit.html", BetEditPageCommand.class);
        commands.put("/user/bet_edit/success.html", BetEditCommand.class);
        commands.put("/user/bet_delete.html", BetDeleteCommand.class);


//        for user
        commands.put("/user/profile.html", ProfilePageCommand.class);
        commands.put("/user/profile/edit.html", ProfileEditCommand.class);
        commands.put("/user/profile/save/edit.html", ProfileEditSaveCommand.class);


        commands.put("/admin/match_add.html", MatchAddPageCommand.class);
        commands.put("/admin/match_add/success.html", MatchAddCommand.class);
        commands.put("/admin/match_edit.html", MatchEditPageCommand.class);
        commands.put("/admin/match_edit/success.html", MatchEditCommand.class);

        commands.put("/broker/assumption.html", AssumptionPageCommand.class);
        commands.put("/broker/assumption_add.html", AssumptionAddPageCommand.class);
        commands.put("/broker/assumption_save.html", AssumptionCommand.class);

        commands.put("/error.html", ErrorPageCommand.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            String contextPath = request.getContextPath();

            String uri = request.getRequestURI();
            Class<? extends Command> commandClass = commands.get(uri);
            try {
                Command command = commandClass.newInstance();
                request.setAttribute("action", command);
                filterChain.doFilter(request, servletResponse);
            } catch (IllegalAccessException | InstantiationException e) {
               logger.error(e);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
