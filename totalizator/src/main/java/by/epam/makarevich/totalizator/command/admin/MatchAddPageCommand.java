package by.epam.makarevich.totalizator.command.admin;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Team;
import by.epam.makarevich.totalizator.service.interfaces.TeamService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MatchAddPageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        try {
            TeamService service = factory.getService(TeamService.class);
            List<Team> teams = service.getAllTeams();
            req.setAttribute("list", teams);
            return new Forward("/admin/matchAdd.jsp");
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(),e);
            return new Forward("error.jsp");
        }

    }
}
