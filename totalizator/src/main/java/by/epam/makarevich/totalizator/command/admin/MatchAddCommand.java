package by.epam.makarevich.totalizator.command.admin;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.model.Team;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;
import by.epam.makarevich.totalizator.service.interfaces.TeamService;
import by.epam.makarevich.totalizator.util.FailChecker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class MatchAddCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Match match = new Match();
        Command.Forward fail = new Command.Forward("/admin/matchAdd.jsp");
        req.setAttribute("error", fail);

        String datetimeLocal = req.getParameter("date").replace("T", " ");

        try {
            TeamService service = factory.getService(TeamService.class);
            Team team1 = service.getTeamByName(req.getParameter("fTeam"));
            Team team2 = service.getTeamByName(req.getParameter("sTeam"));

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date d = sdf.parse(datetimeLocal);
                match.setDate(d);
            } catch (ParseException e) {
                fail.getAttributes().put("dateError", Constant.ERROR_MATCH_DATE);
            }

            if (team1.getId() != team2.getId()) {
                if (team1.getSport() == team2.getSport()) {
                    match.setFirstTeam(team1);
                    match.setSecondTeam(team2);
                } else {
                    System.out.println("team1.getSport() != team2.getSport() "+team1.getSport() + team2.getSport());
                    fail.getAttributes().put("sameTeamError", Constant.ERROR_MATCH_TEAM_SPORT);
                }
            } else {
                fail.getAttributes().put("sameTeamError", Constant.ERROR_MATCH_TEAM);
            }

            if (FailChecker.checkFail(fail)) {
                MatchService matchService = factory.getService(MatchService.class);
                matchService.addMatch(match);
                return new Forward("/", true);
            } else {
                List<Team> teams = service.getAllTeams();
                req.setAttribute("list", teams);
                return fail;
            }
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }

    }
}
