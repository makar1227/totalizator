package by.epam.makarevich.totalizator.command.broker;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class AssumptionPageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        int totalPages = 0;

        String pageParameter = req.getParameter("page");
        int pageIndex = 1;
        int count = 5;
        if (pageParameter != null) {
            try {
                pageIndex = Integer.parseInt(pageParameter);
            } catch (NumberFormatException e) {
                logger.trace("Invalid page parameter passed (" + pageParameter + ")");
            }
        }
        try {
            MatchService service = factory.getService(MatchService.class);
            if (totalPages == 0) {
                totalPages = service.getCountMatchesByStatus(false);
            }
            totalPages = (int) Math.ceil(((double) totalPages) / count);

            List<Match> matches = service.getMatchesByStatus(false, pageIndex, count);
            req.setAttribute("matches", matches);
            req.setAttribute("totalPages", totalPages);
            req.setAttribute("page", pageIndex);
            return new Forward("/broker/assumption.jsp");
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("/error.jsp");
        }
    }
}
