package by.epam.makarevich.totalizator.dao.constants;

public class AssumptionConstant {
    public static final String ID = "id";
    public static final String BROKER_ID= "broker_id";
    public static final String MATCH_ID = "match_id";
    public static final String FIRST_TEAM_WIN = "first_win";
    public static final String SECOND_TEAM_WIN = "second_win";
    public static final String DRAW= "draw";

}
