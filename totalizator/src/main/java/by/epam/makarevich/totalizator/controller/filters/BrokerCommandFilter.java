package by.epam.makarevich.totalizator.controller.filters;

import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Broker command filter.
 */
@WebFilter(urlPatterns = {"/broker/*"})
public class BrokerCommandFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession();
        Authorities authorities = (Authorities) session.getAttribute("authorizedUser");
        if (authorities != null) {
            if (authorities.getRole() == Role.BROKER) {
                filterChain.doFilter(request, servletResponse);
            } else response.sendRedirect(request.getContextPath() + "/error.html");
        } else {
            response.sendRedirect(request.getContextPath() + "/login.html");
        }

    }

    @Override
    public void destroy() {

    }
}
