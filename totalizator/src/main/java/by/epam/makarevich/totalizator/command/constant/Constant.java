package by.epam.makarevich.totalizator.command.constant;


public class Constant {
    public static final String ERROR_REGISTER_BIRTH = "registration.exception.birthday";
    public static final String ERROR_REGISTER_INCORRECT_EMAIL = "registration.exception.email";
    public static final String ERROR_REGISTER_INCORRECT_PHONE = "registration.exception.phone";
    public static final String ERROR_REGISTER_CASH = "registration.exception.cash";
    public static final String ERROR_REGISTER_SIMPLE_PASSWORDS = "registration.exception.simplePassword";
    public static final String ERROR_REGISTER_DIFFERENT_PASSWORDS = "registration.exception.differentPassword";
    public static final String ERROR_REGISTER_INCORRECT_PASSWORD = "registration.exception.incorrectPassword";
    public static final String ERROR_REGISTER_UNDERAGE = "registration.exception.underage";
    public static final String ERROR_REGISTER_EXISTING_EMAIL = "registration.exception.existingEmail";
    public static final String ERROR_REGISTER_EXISTING_LOGIN = "registration.exception.existingLogin";

    public static final String ERROR_BET_OVER_CASH = "user.bet.exception.overCash";
    public static final String ERROR_BET_INCORRECT_SCORE = "user.bet.exception.incorrect.score";
    public static final String ERROR_BET_INCORRECT_CASH = "user.bet.exception.incorrect.cash";
    public static final String ERROR_BET_EXACT_RESULT = "user.bet.exception.exact.failScore";

    public static final String ERROR_LOGIN = "login.exception";
    public static final String ERROR_LOGIN_EMPTY_FIELD = "login.exception.empty";
    public static final String ERROR_MATCH_DATE = "admin.match.exception.incorrectDate";
    public static final String ERROR_MATCH_TEAM = "admin.match.exception.sameTeam";

    public static final String ERROR_MATCH_TEAM_SPORT = "admin.match.exception.differentSport";

}
