package by.epam.makarevich.totalizator.util;

/**
 * The type Score validation.
 */
public class ScoreValidation {
    private static final String SCORE = "\\d+";

    /**
     * Check score boolean.
     *
     * @param score the score
     * @return the boolean
     */
    public static boolean checkScore(String score) {
        if (score.matches(SCORE)) {
            if (Integer.parseInt(score) >= 0) {
                return true;
            }
        }
        return false;
    }
}
