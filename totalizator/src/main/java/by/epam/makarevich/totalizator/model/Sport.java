package by.epam.makarevich.totalizator.model;


/**
 * The enum Sport.
 */
public enum Sport {
    /**
     * Football sport.
     */
    FOOTBALL("football"),
    /**
     * Basketball sport.
     */
    BASKETBALL("basketball"),
    /**
     * Hockey sport.
     */
    HOCKEY("hockey");

    private String sport;

    private Sport(String sport) {
        this.sport = sport;
    }

    /**
     * Gets sport.
     *
     * @return the sport
     */
    public String getSport() {
        return sport;
    }

    /**
     * Gets by id.
     *
     * @param sport the sport
     * @return the by id
     */
    public static int getById(Sport sport) {
        switch (sport) {
            case FOOTBALL:
                return 1;
            case BASKETBALL:
                return 2 ;
            case HOCKEY:
                return 3;
            default:
                return 0;
        }
    }

    /**
     * Get by name sport.
     *
     * @param name the name
     * @return the sport
     */
    public static Sport getByName(String name){
        switch (name){
            case "football":
                return FOOTBALL;
            case "basketball":
                return BASKETBALL;
            default:
                return HOCKEY;
        }
    }
}
