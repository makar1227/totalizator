package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Role;
import by.epam.makarevich.totalizator.service.interfaces.AuthoritiesService;
import by.epam.makarevich.totalizator.util.Crypthographic;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoginCommand extends Command {
    private static Logger logger = Logger.getRootLogger();

    private static Map<Role, List<String>> menu = new ConcurrentHashMap<>();

    static {
        menu.put(Role.ADMIN, new ArrayList<>(Arrays.asList("/admin/match_add.html","/user/profile.html")));
        menu.put(Role.BROKER, new ArrayList<>(Arrays.asList("/broker/assumption.html")));
        menu.put(Role.USER, new ArrayList<>(Arrays.asList("/user/profile.html", "/user/profile/edit.html")));
    }


    @Override
    public Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Command.Forward fail = new Command.Forward("/login.jsp");
        req.setAttribute("error", fail);

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Authorities authorities = new Authorities();
        if (!login.equals("") && !password.equals("")) {
            try {
                AuthoritiesService authoritiesService = factory.getService(AuthoritiesService.class);
                authorities = authoritiesService.getAuthoritiesByLogin(login);

                if (authorities != null && Crypthographic.checkHash(authorities.getPassword(), password)) {
                    HttpSession session = req.getSession();
                    session.setAttribute("authorizedUser", authorities);
                    session.setAttribute("menu", menu.get(authorities.getRole()));
                    return new Forward("/", true);
                } else {
                    fail.getAttributes().put("loginError", Constant.ERROR_LOGIN);
                    req.setAttribute("user", authorities);
                    return fail;
                }
            } catch (NoSuchAlgorithmException | TotalizatorServiceException e) {
                logger.error(e.getMessage(), e);
                return fail;
            }
        } else {
            authorities.setPassword(password);
            authorities.setLogin(login);
            if (login.equals("")) {
                fail.getAttributes().put("loginError", Constant.ERROR_LOGIN_EMPTY_FIELD);
            }
            if (password.equals("")) {
                fail.getAttributes().put("passwordError", Constant.ERROR_LOGIN_EMPTY_FIELD);
            }
            req.setAttribute("user", authorities);
            return fail;
        }
    }
}