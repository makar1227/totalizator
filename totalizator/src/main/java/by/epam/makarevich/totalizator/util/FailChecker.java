package by.epam.makarevich.totalizator.util;

import by.epam.makarevich.totalizator.command.Command;

/**
 * The type Fail checker.
 */
public class FailChecker {
    /**
     * Check fail boolean.
     *
     * @param forward the forward
     * @return the boolean
     */
    public static boolean checkFail(Command.Forward forward) {
        if (forward.getAttributes().size() > 0) {
            return false;
        }
        return true;
    }
}
