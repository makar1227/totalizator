package by.epam.makarevich.totalizator.util;


/**
 * The type Cash validator.
 */
public class CashValidator {
    private static final String CASH = "\\d+(\\.\\d{0,2})?";

    /**
     * Check cash boolean.
     *
     * @param cash the cash
     * @return the boolean
     */
    public static boolean checkCash(String cash) {
        if (cash.matches(CASH)) {
            if(Double.parseDouble(cash)>0)
            return true;
        }
        return false;
    }
}
