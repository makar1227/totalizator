package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Team;

import java.util.List;

/**
 * The interface Team dao.
 */
public interface TeamDao  {

    Team getTeamByName(String login) throws TotalizatorSqlException;

    Team getTeamById(int id) throws TotalizatorSqlException;

    List<Team> getAllTeams() throws TotalizatorSqlException;

}
