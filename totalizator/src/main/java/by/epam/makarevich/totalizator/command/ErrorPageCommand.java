package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The type Error page command.
 */
public class ErrorPageCommand extends Command{
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) throws TotalizatorSqlException {
        return new Forward("/error.jsp");
    }
}
