package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Assumption;

import java.util.List;

public interface AssumptionService extends Service {

    Assumption getAssumptionByMatchId(int matchId) throws TotalizatorServiceException;

    boolean addAssumption(Assumption assumption) throws TotalizatorServiceException;

    List<Assumption> getValidAssumptions(boolean valid,  int start, int count) throws TotalizatorServiceException;

    int getCountValidAssumption(boolean valid) throws TotalizatorServiceException;

}

