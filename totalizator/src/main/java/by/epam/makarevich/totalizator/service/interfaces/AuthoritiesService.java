package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;

public interface AuthoritiesService extends Service {

    Authorities getAuthoritiesById(int id) throws TotalizatorServiceException;

    Authorities getAuthoritiesByLogin(String login) throws TotalizatorServiceException;

    boolean addAuthority(Authorities authorities) throws TotalizatorServiceException;

    void editAuthority(Authorities authorities) throws TotalizatorServiceException;
}
