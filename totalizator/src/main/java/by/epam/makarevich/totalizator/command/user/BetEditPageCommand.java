package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.service.interfaces.BetService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BetEditPageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        try {
            BetService betService = factory.getService(BetService.class);
            Bet bet = betService.getBetById(Integer.parseInt(req.getParameter("id")));
            req.setAttribute("bet", bet);
            req.setAttribute("firstTeamName", req.getParameter("firstTeamName"));
            req.setAttribute("secondTeamName", req.getParameter("secondTeamName"));
            return new Forward("betEdit.jsp");
        }catch (TotalizatorServiceException e){
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }
    }
}
