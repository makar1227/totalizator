package by.epam.makarevich.totalizator.util;

/**
 * The type Phone validator.
 */
public class PhoneValidator {
    private final static String PHONE = "^\\+\\d{5}-\\d{3}-\\d{2}-\\d{2}$";

    /**
     * Check phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean checkPhone(String phone) {
        return phone.matches(PHONE);
    }
}
