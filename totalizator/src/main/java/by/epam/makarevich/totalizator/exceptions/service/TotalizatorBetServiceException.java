package by.epam.makarevich.totalizator.exceptions.service;


public class TotalizatorBetServiceException extends TotalizatorServiceException{
    public static final int HAVE_NO_SUCH_MONEY = 1;

    private int code;
    public TotalizatorBetServiceException(Throwable cause) {
        super(cause);
    }

    public TotalizatorBetServiceException(int code) {
        this.code = code;
    }
    public TotalizatorBetServiceException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
