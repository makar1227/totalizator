package by.epam.makarevich.totalizator.util;

/**
 * The type Password validator.
 */
public class PasswordValidator {
    private final static String PASSWORD= "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$";

    /**
     * Check password boolean.
     *
     * @param pasword the pasword
     * @return the boolean
     */
    public static boolean checkPassword(String pasword) {
        return pasword.matches(PASSWORD);
    }
}
