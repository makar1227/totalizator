package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.BrokerConstant;
import by.epam.makarevich.totalizator.dao.interfaces.BrokerDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Broker;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


/**
 * The type Broker dao.
 */
public class BrokerDaoImpl implements BrokerDao {
    private static Logger logger = Logger.getLogger(BrokerDaoImpl.class);

    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM totalizator.broker WHERE id=?";
    private static final String QUERY_SELECT_BY_LOGIN = "SELECT * FROM totalizator.broker WHERE login=?";


    private List<Broker> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Broker> brokers = new LinkedList<>();
        try {
            while (result.next()) {
                Broker broker = new Broker();
                broker.setId(result.getInt(BrokerConstant.ID));
                broker.setLogin(result.getString(BrokerConstant.LOGIN));
                broker.setPassword(result.getString(BrokerConstant.PASSWORD));
                brokers.add(broker);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Broker resultSet.", e);
        }
        return brokers;
    }

    @Override
    public Broker getBrokerById(int id) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_ID;
        List<Broker> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting broker by id ", e);
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public Broker getBrokerByLogin(String login) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_LOGIN;
        List<Broker> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Broker by login.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    public BrokerDaoImpl() {
        super();
    }
}
