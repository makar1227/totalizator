package by.epam.makarevich.totalizator.dao.constants;

public class TeamConstant {
    public static final String ID = "id";
    public static final String NAME= "team_name";
    public static final String COUNTRY = "country";
    public static final String CITY = "city";
    public static final String SPORT = "sport";

}
