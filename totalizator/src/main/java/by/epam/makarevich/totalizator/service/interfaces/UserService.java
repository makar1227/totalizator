package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.User;

import java.util.List;


public interface UserService extends Service {

    User getUserById(int id) throws TotalizatorServiceException;

    boolean addUser(User user) throws TotalizatorServiceException;

    void editUser(User user) throws TotalizatorServiceException;

    User getUserByLogin(String login) throws TotalizatorServiceException;

    User getUserByEmail(String email) throws TotalizatorServiceException;
}
