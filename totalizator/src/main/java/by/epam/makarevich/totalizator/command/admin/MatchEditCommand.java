package by.epam.makarevich.totalizator.command.admin;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.model.Result;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MatchEditCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {

        try {
            MatchService service = factory.getService(MatchService.class);
            Match match = service.getMatchById(Integer.parseInt(req.getParameter("id")));
            match.setfResult(Integer.parseInt(req.getParameter("fScore")));
            match.setsResult(Integer.parseInt(req.getParameter("sScore")));
            if (match.getfResult() == match.getsResult()) {
                match.setResult(Result.DRAW);
            } else if (match.getfResult() > match.getsResult()) {
                match.setResult(Result.FIRST_WIN);
            } else match.setResult(Result.SECOND_WIN);

            match.setMatchStatus(false);
            service.editMatch(match);
            return new Forward("/", true);
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(),e);
            return new Forward("error.jsp");
        }
    }
}
