package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.service.interfaces.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Command.
 */
public abstract class Command {
    /**
     * The constant logger.
     */
    protected static Logger logger = Logger.getRootLogger();
    /**
     * The Factory.
     */
    protected ServiceFactory factory;
    /**
     * Gets factory.
     *
     * @return the factory
     */
    public ServiceFactory getFactory() {
        return factory;
    }
    /**
     * Sets factory.
     *
     * @param factory the factory
     */
    public void setFactory(ServiceFactory factory) {
        this.factory = factory;
    }
    /**
     * Exec command . forward.
     *
     * @param req  the req
     * @param resp the resp
     * @return the command . forward
     * @throws TotalizatorSqlException     the totalizator sql exception
     * @throws TotalizatorServiceException the totalizator service exception
     */
    public abstract Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) throws TotalizatorSqlException, TotalizatorServiceException;

    /**
     * The type Forward.
     */
    public static class Forward {
        private String forward;
        private boolean redirect;
        private Map<String, Object> attributes = new HashMap<>();

        /**
         * Instantiates a new Forward.
         *
         * @param forward  the forward
         * @param redirect the redirect
         */
        public Forward(String forward, boolean redirect) {
            this.forward = forward;
            this.redirect = redirect;
        }

        /**
         * Instantiates a new Forward.
         *
         * @param forward the forward
         */
        public Forward(String forward) {
            this(forward, false);
        }

        /**
         * Gets forward.
         *
         * @return the forward
         */
        public String getForward() {
            return forward;
        }

        /**
         * Sets forward.
         *
         * @param forward the forward
         */
        public void setForward(String forward) {
            this.forward = forward;
        }

        /**
         * Is redirect boolean.
         *
         * @return the boolean
         */
        public boolean isRedirect() {
            return redirect;
        }

        /**
         * Sets redirect.
         *
         * @param redirect the redirect
         */
        public void setRedirect(boolean redirect) {
            this.redirect = redirect;
        }

        /**
         * Gets attributes.
         *
         * @return the attributes
         */
        public Map<String, Object> getAttributes() {
            return attributes;
        }

    }
}
