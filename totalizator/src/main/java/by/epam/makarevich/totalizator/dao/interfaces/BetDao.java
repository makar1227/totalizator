package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.User;

import java.util.List;

public interface BetDao {

    Bet getBetById(int betId) throws TotalizatorSqlException;

    boolean addBet(Bet bet, User user) throws TotalizatorSqlException;

    void deleteBetById(int betId) throws TotalizatorSqlException;

    void editBet(Bet bet, User user) throws TotalizatorSqlException;

    List<Bet> getUserBets(int userId, int start, int count) throws TotalizatorSqlException;

    int getCountUserBets(int userId) throws TotalizatorSqlException;

    List<Bet> getMatchBets(int matchId) throws TotalizatorSqlException;

}
