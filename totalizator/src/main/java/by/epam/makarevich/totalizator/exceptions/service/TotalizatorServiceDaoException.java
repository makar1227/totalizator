package by.epam.makarevich.totalizator.exceptions.service;

/**
 * Created by makar1227 on 5/11/2017.
 */
public class TotalizatorServiceDaoException extends TotalizatorServiceException{
    public TotalizatorServiceDaoException() {
    }

    public TotalizatorServiceDaoException(String message) {
        super(message);
    }

    public TotalizatorServiceDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public TotalizatorServiceDaoException(Throwable cause) {
        super(cause);
    }
}
