package by.epam.makarevich.totalizator.command.pages;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginPageCommand extends Command {

    @Override
    public Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        Authorities authorities = (Authorities) session.getAttribute("authorizedUser");
        if (authorities == null) {
            return new Forward("/login.jsp");
        }
        return new Forward("/",true);
    }
}

