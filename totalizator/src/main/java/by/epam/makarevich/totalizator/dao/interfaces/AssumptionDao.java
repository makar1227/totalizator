package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Assumption;

import java.util.List;

public interface AssumptionDao {
    List<Assumption> getValidAssumption(boolean valid, int start, int count) throws TotalizatorSqlException;

    Assumption getAssumptionByMatchId(int matchId) throws TotalizatorSqlException;

    int getCountValidAssumption(boolean valid) throws TotalizatorSqlException;

    boolean addAssumption(Assumption assumption) throws TotalizatorSqlException;
}
