package by.epam.makarevich.totalizator.util;

/**
 * The type Email validator.
 */
public class EmailValidator {
    private static final String EMAIl = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Validate email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public static boolean validateEmail(String email) {
        return email.matches(EMAIl);
    }
}
