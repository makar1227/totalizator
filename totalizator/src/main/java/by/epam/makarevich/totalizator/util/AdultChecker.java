package by.epam.makarevich.totalizator.util;


import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

/**
 * The type Adult checker.
 */
public class AdultChecker {
    /**
     * Check adult boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public static boolean checkAdult(Date date) {
        LocalDate birth = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate now = LocalDate.now();
        Period p = Period.between(birth, now);
        if (p.getYears() >= 18) {
            return true;
        }
        return false;
    }
}
