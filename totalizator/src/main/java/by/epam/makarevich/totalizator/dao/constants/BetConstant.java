package by.epam.makarevich.totalizator.dao.constants;

public class BetConstant {

    public static final String ID = "id";
    public static final String USER_ID= "user_id";
    public static final String MATCH_ID = "match_id";
    public static final String FIRST_TEAM_SCORE= "score_first_team";
    public static final String SECOND_TEAM_SCORE = "score_second_team";
    public static final String RESULT= "result_common";
    public static final String CASH= "amount";

}
