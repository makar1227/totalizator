package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Entity;

import java.util.List;


public interface Dao<T extends Entity> {

    boolean add(T entity) throws TotalizatorSqlException;

    T getById(int id) throws TotalizatorSqlException;

    List<T> getAll() throws TotalizatorSqlException;

    void deleteById(int id) throws TotalizatorSqlException;

    void edit(T entity) throws TotalizatorSqlException;

}
