package by.epam.makarevich.totalizator.exceptions.service;

/**
 * Created by makar1227 on 5/11/2017.
 */
public class TotalizatorServiceException  extends Exception{
    public TotalizatorServiceException() {
    }

    public TotalizatorServiceException(String message) {
        super(message);
    }

    public TotalizatorServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public TotalizatorServiceException(Throwable cause) {
        super(cause);
    }
}
