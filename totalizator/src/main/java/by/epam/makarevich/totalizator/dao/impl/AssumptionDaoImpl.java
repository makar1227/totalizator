package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.AssumptionConstant;
import by.epam.makarevich.totalizator.dao.constants.MatchConstant;
import by.epam.makarevich.totalizator.dao.constants.TeamConstant;
import by.epam.makarevich.totalizator.dao.interfaces.AssumptionDao;
import by.epam.makarevich.totalizator.dao.interfaces.MatchDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.*;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Assumption dao.
 */

public class AssumptionDaoImpl implements AssumptionDao {

    private static Logger logger = Logger.getLogger(AssumptionDaoImpl.class);

    private static final String QUERY_INSERT = "INSERT INTO totalizator.brokerassumption (broker_id, match_id, first_win, draw,second_win) VALUES (?,?,?,?,?)";

    private static final String QUERY_SELECT_BY_MATCH_ID = "SELECT * FROM totalizator.brokerassumption WHERE match_id=?";
    private static final String QUERY_SELECT_MATCHES_WITH_ASSUMPTION = "SELECT  M.id, M.match_date, \n" +
            "T1.sport, T1.team_name T1Name, T2.team_name T2Name, \n" +
            " A.first_win, A.draw, A.second_win \n" +
            "FROM totalizator.brokerassumption A \n" +
            "INNER join totalizator.match M\n" +
            "ON A.match_id=M.id INNER JOIN totalizator.team T1 ON M.first_team_id=T1.id\n" +
            "INNER JOIN totalizator.team T2 ON M.second_team_id=T2.id\n" +
            "WHERE M.match_status=? AND M.match_date > CURRENT_TIMESTAMP ORDER BY M.match_date LIMIT ?, ?";

    private static final String QUERY_COUNT_MATCHES_WITH_ASSUMPTION = "SELECT  COUNT(*) \n" +
            "FROM totalizator.brokerassumption A \n" +
            "INNER join totalizator.match M \n" +
            "ON A.match_id=M.id INNER JOIN totalizator.team T1 ON M.first_team_id=T1.id\n" +
            "INNER JOIN totalizator.team T2 ON M.second_team_id=T2.id\n" +
            "WHERE M.match_status=? AND M.match_date > CURRENT_TIMESTAMP";




    private List<Assumption> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Assumption> assumptions = new LinkedList<>();
        try {
            while (result.next()) {
                Assumption assumption = new Assumption();
                Match match = new Match();
                Broker broker = new Broker();
                assumption.setId(result.getInt(AssumptionConstant.ID));
                broker.setId(result.getInt(AssumptionConstant.BROKER_ID));
                assumption.setBroker(broker);
                match.setId(result.getInt(AssumptionConstant.MATCH_ID));
                assumption.setMatch(match);
                assumption.setfWin(result.getDouble(AssumptionConstant.FIRST_TEAM_WIN));
                assumption.setDraw(result.getDouble(AssumptionConstant.DRAW));
                assumption.setsWin(result.getDouble(AssumptionConstant.SECOND_TEAM_WIN));
//
                assumptions.add(assumption);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Assumption resultSet.", e);
        }
        return assumptions;
    }

    private void prepareStatementForInsert(PreparedStatement statement, Assumption assumption) throws TotalizatorSqlException {
        try {
            statement.setInt(1, assumption.getBroker().getId());
            statement.setInt(2, assumption.getMatch().getId());
            statement.setDouble(3, assumption.getfWin());
            statement.setDouble(4, assumption.getDraw());
            statement.setDouble(5, assumption.getsWin());
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for insert Assumption.", e);
        }
    }

    @Override
    public List<Assumption> getValidAssumption(boolean valid, int start, int count) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_MATCHES_WITH_ASSUMPTION;
        List<Assumption> assumptions = new ArrayList<>();
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, valid);
            statement.setInt(2, start);
            statement.setInt(3, count);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Assumption assumption = new Assumption();
                    Match match = new Match();
                    Team team1 = new Team();
                    Team team2 = new Team();
                    match.setId(result.getInt(MatchConstant.ID));
                    match.setDate(result.getTimestamp(MatchConstant.DATE));
                    assumption.setMatch(match);
                    team1.setName(result.getString(MatchConstant.FIRST_TEAM_NAME));
                    Sport sport = Sport.getByName(result.getString(TeamConstant.SPORT));
                    team1.setSport(sport);
                    team2.setName(result.getString(MatchConstant.SECOND_TEAM_NAME));
                    assumption.getMatch().setFirstTeam(team1);
                    assumption.getMatch().setSecondTeam(team2);
                    assumption.setfWin(result.getDouble(AssumptionConstant.FIRST_TEAM_WIN));
                    assumption.setDraw(result.getDouble(AssumptionConstant.DRAW));
                    assumption.setsWin(result.getDouble(AssumptionConstant.SECOND_TEAM_WIN));

                    assumptions.add(assumption);
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting valid Assumptions.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return assumptions;

    }

    @Override
    public Assumption getAssumptionByMatchId(int matchId) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_MATCH_ID;
        List<Assumption> assumptions;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, matchId);
            try (ResultSet result = statement.executeQuery()) {
                assumptions = parseResultSet(result);
                if (assumptions.size() != 1 || assumptions == null) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting entity by id ", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return assumptions.iterator().next();

    }

    @Override
    public int getCountValidAssumption(boolean valid) throws TotalizatorSqlException {

        String sql = QUERY_COUNT_MATCHES_WITH_ASSUMPTION;
        int count = 0;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, valid);
            try (ResultSet result = statement.executeQuery()) {
                result.next();
                count = result.getInt(1);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with counting valid Assumptions.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }

        return count;
    }

    @Override
    public boolean addAssumption(Assumption assumption) throws TotalizatorSqlException {
        String sql = QUERY_INSERT;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, assumption);
            MatchDao matchDao=new MatchDaoImpl();
            Match match=matchDao.getMatchById(assumption.getMatch().getId());
            match.setMatchStatus(true);

            connection.setAutoCommit(false);
            int count = statement.executeUpdate();
            matchDao.editMatchStatus(connection,match);
            connection.commit();

            if (count == 1) {
                logger.info("Assumption was successfully created!");
            } else {
                logger.error("Assumption wasn't added ");
                throw new TotalizatorSqlException("Assumption wasn't added ");
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new TotalizatorSqlException("Can not make an assumption object in DB, rollback operation", e);
            }
            throw new TotalizatorSqlException("Cannot insert assumption object in DB", e);
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return true;
    }

    public AssumptionDaoImpl() {
        super();
    }
}
