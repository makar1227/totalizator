package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.TeamConstant;
import by.epam.makarevich.totalizator.dao.interfaces.TeamDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Sport;
import by.epam.makarevich.totalizator.model.Team;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


/**
 * The type Team dao.
 */
public class TeamDaoImpl implements TeamDao {
    private static Logger logger = Logger.getLogger(TeamDaoImpl.class);

    private static final String QUERY_SELECT_ALL = "SELECT * FROM totalizator.team";
    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM totalizator.team WHERE id=?";
    private static final String QUERY_SELECT_BY_NAME = "SELECT * FROM  totalizator.team WHERE team_name=?";


    private List<Team> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Team> teams = new LinkedList<>();
        try {
            while (result.next()) {
                Team team = new Team();
                team.setId(result.getInt(TeamConstant.ID));
                team.setName(result.getString(TeamConstant.NAME));
                team.setCountry(result.getString(TeamConstant.COUNTRY));
                team.setCity(result.getString(TeamConstant.CITY));
                Sport sport = Sport.getByName(result.getString(TeamConstant.SPORT));
                team.setSport(sport);
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Team resultSet.", e);
        }
        return teams;
    }


    @Override
    public Team getTeamByName(String login) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_NAME;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        List<Team> list = null;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Team by name.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public Team getTeamById(int id) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_ID;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        List<Team> list = null;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Team by id.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public List<Team> getAllTeams() throws TotalizatorSqlException {
        String sql = QUERY_SELECT_ALL;
        List<Team> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting all teams ", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list;
    }

    public TeamDaoImpl() {
        super();
    }
}
