package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorBetServiceException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.model.Result;
import by.epam.makarevich.totalizator.service.interfaces.BetService;
import by.epam.makarevich.totalizator.util.CashValidator;
import by.epam.makarevich.totalizator.util.FailChecker;
import by.epam.makarevich.totalizator.util.ScoreValidation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BetEditCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Command.Forward fail = new Command.Forward("/betEdit.jsp");
        req.setAttribute("error", fail);

        String fScore = req.getParameter("fScore");
        String sScore = req.getParameter("sScore");
        String cash = req.getParameter("cash");

        try {

            BetService betService = factory.getService(BetService.class);
            Bet bet = betService.getBetById(Integer.parseInt(req.getParameter("betId")));
            bet.setResult(Result.getByName(req.getParameter("result")));

            Match match = new Match();
            match.setId(Integer.parseInt(req.getParameter("matchId")));

            if (!req.getParameter("fScore").equals("") && !req.getParameter("sScore").equals("")) {
                if (ScoreValidation.checkScore(fScore)) {
                    bet.setFirstTeamScore(Integer.parseInt(fScore));
                } else {
                    fail.getAttributes().put("fScoreError", Constant.ERROR_BET_INCORRECT_SCORE);
                }
                if (ScoreValidation.checkScore(sScore)) {
                    bet.setSecondTeamScore(Integer.parseInt(sScore));
                } else {
                    fail.getAttributes().put("sScoreError", Constant.ERROR_BET_INCORRECT_SCORE);
                }

                if (ScoreValidation.checkScore(fScore) && ScoreValidation.checkScore(sScore)) {
                    switch (bet.getResult()) {
                        case DRAW:
                            if (!fScore.equals(sScore)) {
                                fail.getAttributes().put("scoreError", Constant.ERROR_BET_EXACT_RESULT);
                            }
                            break;
                        case FIRST_WIN:
                            if (Integer.parseInt(fScore) <= Integer.parseInt(sScore)) {
                                fail.getAttributes().put("scoreError", Constant.ERROR_BET_EXACT_RESULT);
                            }
                            break;
                        case SECOND_WIN:
                            if (Integer.parseInt(fScore) >= Integer.parseInt(sScore)) {
                                fail.getAttributes().put("scoreError", Constant.ERROR_BET_EXACT_RESULT);
                            }
                            break;
                    }
                }
            }
            if (CashValidator.checkCash(cash)) {
                bet.setCashSum(Double.parseDouble(cash));
            } else {
                fail.getAttributes().put("cashError", Constant.ERROR_BET_INCORRECT_CASH);
            }

            if (FailChecker.checkFail(fail)) {
                try {
                    betService.editBet(bet);
                    return new Forward("/user/profile.html", true);
                } catch (TotalizatorBetServiceException e) {
                    fail.getAttributes().put("overCashError", e.getMessage());
                    req.setAttribute("bet", bet);
                    req.setAttribute("match", match);
                    req.setAttribute("firstTeamName", req.getParameter("firstTeamName"));
                    req.setAttribute("secondTeamName", req.getParameter("secondTeamName"));
                    return fail;
                }
            } else {
                req.setAttribute("bet", bet);
                req.setAttribute("match", match);
                req.setAttribute("firstTeamName", req.getParameter("firstTeamName"));
                req.setAttribute("secondTeamName", req.getParameter("secondTeamName"));
                return fail;
            }
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }
    }
}
