package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.dao.interfaces.BetDao;
import by.epam.makarevich.totalizator.dao.interfaces.UserDao;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorBetServiceException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.interfaces.BetService;

import java.util.List;

/**
 * The type Bet service.
 */
public class BetServiceImpl extends ServiceImpl implements BetService {
    @Override
    public Bet getBetById(int id) throws TotalizatorServiceException {
        try {
            return factory.getBetDao().getBetById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public List<Bet> getMatchBets(int matchId) throws TotalizatorServiceException {
        try {
            return factory.getBetDao().getMatchBets(matchId);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public List<Bet> getUserBets(int userId, int start, int count) throws TotalizatorServiceException {
        if ((start - 1) * count < 0) {
            start = 1;
        }
        int index = (start - 1) * count;

        try {
            return factory.getBetDao().getUserBets(userId, index, count);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public int getCountUserBets(int userId) throws TotalizatorServiceException {
        try {
            return factory.getBetDao().getCountUserBets(userId);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public boolean addBet(Bet bet) throws TotalizatorServiceException {

        boolean create = false;
        try {
            BetDao betDao = factory.getBetDao();
            UserDao userDao = factory.getUserDao();
            User user = userDao.getUserById(bet.getUser().getId());
            if (user.getCashAmount() < bet.getCashSum()) {
                throw new TotalizatorBetServiceException(Constant.ERROR_BET_OVER_CASH, TotalizatorBetServiceException.HAVE_NO_SUCH_MONEY);
            } else {
                user.setCashAmount(user.getCashAmount() - bet.getCashSum());
                create = betDao.addBet(bet, user);
            }
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
        return create;
    }

    @Override
    public void deleteBet(int id) throws TotalizatorServiceException {
        try {
            factory.getBetDao().deleteBetById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in Bet service layer.", e);
        }
    }

    @Override
    public void editBet(Bet bet) throws TotalizatorServiceException {
        try {
            BetDao betDao = factory.getBetDao();
            UserDao userDao = factory.getUserDao();
            User user = userDao.getUserById(bet.getUser().getId());
            Bet oldBet = betDao.getBetById(bet.getId());
            user.setCashAmount(user.getCashAmount() + oldBet.getCashSum() - bet.getCashSum());

            if (user.getCashAmount() < bet.getCashSum()) {
                throw new TotalizatorBetServiceException(Constant.ERROR_BET_OVER_CASH, TotalizatorBetServiceException.HAVE_NO_SUCH_MONEY);
            } else {
                betDao.editBet(bet,user);
            }
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in Bet service layer.", e);

        }
    }
}
