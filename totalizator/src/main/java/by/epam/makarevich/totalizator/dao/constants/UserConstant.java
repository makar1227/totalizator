package by.epam.makarevich.totalizator.dao.constants;

public class UserConstant {
    public static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String EMAIL = "mail";
    public static final String BIRTH = "birth";
    public static final String CASH = "cash_amount";
}
