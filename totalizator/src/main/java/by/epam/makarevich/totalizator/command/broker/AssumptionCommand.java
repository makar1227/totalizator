package by.epam.makarevich.totalizator.command.broker;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Assumption;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Broker;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.service.interfaces.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AssumptionCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        try {
            Assumption assumption = new Assumption();

            assumption.setfWin(Double.parseDouble(req.getParameter("fScore")));
            assumption.setsWin(Double.parseDouble(req.getParameter("sScore")));
            assumption.setDraw(Double.parseDouble(req.getParameter("draw")));

            HttpSession session = req.getSession();
            Authorities authorities = (Authorities) session.getAttribute("authorizedUser");

            BrokerService service = factory.getService(BrokerService.class);
            Broker broker = service.getBrokerByLogin(authorities.getLogin());
            assumption.setBroker(broker);

            Match match = new Match();
            match.setId(Integer.parseInt(req.getParameter("match")));
            assumption.setMatch(match);

            AssumptionService assumptionService = factory.getService(AssumptionService.class);
            assumptionService.addAssumption(assumption);
            return new Forward("/broker/assumption.html", true);
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }
    }
}
