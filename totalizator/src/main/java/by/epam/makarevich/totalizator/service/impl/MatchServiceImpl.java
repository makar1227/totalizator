package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.dao.interfaces.*;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.*;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;

import java.util.List;

/**
 * The type Match service.
 */
public class MatchServiceImpl extends ServiceImpl implements MatchService {
    @Override
    public Match getMatchById(int id) throws TotalizatorServiceException {
        try {
            MatchDao matchDao = factory.getMatchDao();
            Match match = (matchDao.getMatchById(id));
            TeamDao teamDao = factory.getTeamDao();
            match.setFirstTeam(teamDao.getTeamById(match.getFirstTeam().getId()));
            match.setSecondTeam((teamDao.getTeamById(match.getSecondTeam().getId())));
            return match;
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public boolean addMatch(Match match) throws TotalizatorServiceException {
        try {
            return  factory.getMatchDao().addMatch(match);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public void editMatch(Match match) throws TotalizatorServiceException {
        double exactCoef = 1.5;
        try {
            MatchDao matchDao = factory.getMatchDao();
           // UserDao userDao = factory.getUserDao();
            AssumptionDao assumptionDao = factory.getAssumptionDao();

            Assumption assumption = assumptionDao.getAssumptionByMatchId(match.getId());
            System.out.println("assump "+assumption.toString());
            BetDao betDao =  factory.getBetDao();
            List<Bet> bets = betDao.getMatchBets(match.getId());
            System.out.println("bets "+bets.toString());
          //  User admin = userDao.getUserByLogin("admin");

//            factory.setAutoCommit(connection, false);

//            for (Bet bet : bets) {
//                double cash = 0;
//                User user = userDao.getUserById(bet.getUser().getId());
//                if (bet.getResult() == match.getResult()) {
//                    switch (bet.getResult()) {
//                        case FIRST_WIN:
//                            cash = bet.getCashSum() * assumption.getfWin();
//                            break;
//                        case DRAW:
//                            cash = bet.getCashSum() * assumption.getDraw();
//                            break;
//                        case SECOND_WIN:
//                            cash = bet.getCashSum() * assumption.getsWin();
//                            break;
//                    }
//                    if (bet.getFirstTeamScore() == match.getfResult() && bet.getSecondTeamScore() == match.getsResult()) {
//                        cash = cash * exactCoef;
//                    }
//                    user.setCashAmount(user.getCashAmount() + cash);
//                    admin.setCashAmount(admin.getCashAmount() - (cash - bet.getCashSum()));
//                } else {
//                    admin.setCashAmount(admin.getCashAmount() + bet.getCashSum());
//                }
//                userDao.editUser(user);
//                userDao.editUser(admin);
//            }
            matchDao.editMatch(match, assumption, bets);
//            factory.commit(connection);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public List<Match> getMatchesByStatus(boolean isValid, int start, int count) throws TotalizatorServiceException {
        if ((start - 1) * count < 0) {
            start = 1;
        }
        int index = (start - 1) * count;

        try {
            return factory.getMatchDao().getMatchesByStatus(isValid, index, count);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public int getCountMatchesByStatus(boolean isValid) throws TotalizatorServiceException {
        try {
            return factory.getMatchDao().getCountMatchesByStatus(isValid);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
