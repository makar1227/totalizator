package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;


/**
 * The interface Service factory.
 */
public interface ServiceFactory {
    /**
     * Gets service.
     *
     * @param <T>    the type parameter
     * @param entity the entity
     * @return the service
     * @throws TotalizatorServiceException the totalizator service exception
     */
    <T extends Service> T getService(Class<? extends Service> entity) throws  TotalizatorServiceException ;
}
