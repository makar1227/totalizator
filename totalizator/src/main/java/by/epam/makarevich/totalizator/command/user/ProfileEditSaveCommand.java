package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.dao.impl.MySqlDaoFactory;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorUserServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.impl.ServiceFactoryImpl;
import by.epam.makarevich.totalizator.service.interfaces.BetService;
import by.epam.makarevich.totalizator.service.interfaces.ServiceFactory;
import by.epam.makarevich.totalizator.service.interfaces.UserService;
import by.epam.makarevich.totalizator.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.List;


public class ProfileEditSaveCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Command.Forward fail = new Command.Forward("/profileEdit.jsp");
        req.setAttribute("error", fail);

        HttpSession session = req.getSession();
        List<Bet> bets = null;
        Authorities authorities = (Authorities) session.getAttribute("authorizedUser");
        User user = null;

        String password = req.getParameter("password");
        String newPassword = req.getParameter("newPassword");
        String email = req.getParameter("email");
        String cash = req.getParameter("cash");
        String phone = req.getParameter("phone");

        try {
            UserService userService = factory.getService(UserService.class);
            user = userService.getUserByLogin(authorities.getLogin());

            try {
                if (Crypthographic.checkHash(user.getPassword(), password)) {
                    if (PasswordValidator.checkPassword(newPassword)) {
                        user.setPassword(newPassword);
                        authorities.setPassword(Crypthographic.getHash(newPassword));
                    } else {
                        fail.getAttributes().put("simplePasswordError", Constant.ERROR_REGISTER_SIMPLE_PASSWORDS);
                    }
                } else {
                    fail.getAttributes().put("passwordError", Constant.ERROR_REGISTER_INCORRECT_PASSWORD);
                }
            } catch (NoSuchAlgorithmException e) {
                throw new TotalizatorUserServiceException("Internal error", TotalizatorUserServiceException.INTERNAL_ERROR);
            }
            if (EmailValidator.validateEmail(email)) {
                user.setEmail(email);
            } else {
                fail.getAttributes().put("emailError", Constant.ERROR_REGISTER_INCORRECT_EMAIL);

            }
            if (PhoneValidator.checkPhone(phone)) {
                user.setPhone(phone);
            } else {
                fail.getAttributes().put("phoneError", Constant.ERROR_REGISTER_INCORRECT_PHONE);

            }
            if (CashValidator.checkCash(cash)) {
                user.setCashAmount(Double.valueOf(cash));
            } else {
                fail.getAttributes().put("cashError", Constant.ERROR_REGISTER_CASH);
            }

            if (FailChecker.checkFail(fail)) {
                try {
                    userService.editUser(user);
                } catch (TotalizatorUserServiceException e) {
                    switch (e.getCode()) {
                        case TotalizatorUserServiceException.EMAIL_EXISTS:
                            fail.getAttributes().put("emailError", e.getMessage());
                            break;
                    }
                }

                session.setAttribute("authorizedUser", authorities);
                return new Forward("/user/profile.html", true);
            }

        } catch (TotalizatorServiceException e) {
            logger.fatal("Exception occurred in database during user date editing", e);
            return new Forward("error.jsp");
        }
        req.setAttribute("user", user);
        return fail;
    }
}
