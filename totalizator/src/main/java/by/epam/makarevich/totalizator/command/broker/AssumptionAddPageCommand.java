package by.epam.makarevich.totalizator.command.broker;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AssumptionAddPageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("match", req.getParameter("match"));
        req.setAttribute("fTeam", req.getParameter("fTeam"));
        req.setAttribute("sTeam", req.getParameter("sTeam"));

        return new Forward("/broker/addAssumption.jsp");
    }
}
