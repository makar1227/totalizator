package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.dao.impl.MySqlDaoFactory;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.service.interfaces.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The type Service factory.
 */
public class ServiceFactoryImpl implements ServiceFactory {
    private MySqlDaoFactory factory;

    private static final Map<Class<? extends Service>, Class<? extends ServiceImpl>> SERVICES = new ConcurrentHashMap<>();

    static {
        SERVICES.put(AssumptionService.class, AssumptionServiceImpl.class);
        SERVICES.put(AuthoritiesService.class, AuthoritiesServiceImpl.class);
        SERVICES.put(BetService.class, BetServiceImpl.class);
        SERVICES.put(BrokerService.class, BrokerServiceImpl.class);
        SERVICES.put(MatchService.class, MatchServiceImpl.class);
        SERVICES.put(TeamService.class, TeamServiceImpl.class);
        SERVICES.put(UserService.class, UserServiceImpl.class);
    }

    @Override
    public <T extends Service> T getService(Class<? extends Service> key) throws TotalizatorServiceException {

        Class<? extends ServiceImpl> value=SERVICES.get(key);

        if(value!=null){
            try{
                ServiceImpl service=value.newInstance();
                service.setDaoFactory(factory);
                return (T)service;
            }catch (InstantiationException |IllegalAccessException e){
                throw new TotalizatorServiceException(e);
            }
        }
        return null;
    }


    public ServiceFactoryImpl(MySqlDaoFactory factory) {
        this.factory = factory;
    }
}
