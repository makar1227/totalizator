package by.epam.makarevich.totalizator.dao.pool;

public class DatabaseProperty {
    public static final String DRIVER_NAME = "db.driver";

    public static final String USER = "db.user";

    public static final String PASSWORD = "db.password";

    public static final String POOL_SIZE = "db.poolsize";

    public static final String URL = "db.url";

    private DatabaseProperty() {
    }
}
