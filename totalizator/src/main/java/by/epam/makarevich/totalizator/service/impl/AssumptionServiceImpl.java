package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.dao.interfaces.AssumptionDao;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Assumption;
import by.epam.makarevich.totalizator.service.interfaces.AssumptionService;

import java.util.List;


/**
 * The type Assumption service.
 */
public class AssumptionServiceImpl extends ServiceImpl implements AssumptionService {

    @Override
    public Assumption getAssumptionByMatchId(int matchId) throws TotalizatorServiceException {
        try {
            AssumptionDao assumptionDao = factory.getAssumptionDao();
            Assumption assumption = assumptionDao.getAssumptionByMatchId(matchId);
            return assumption;
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public boolean addAssumption(Assumption assumption) throws TotalizatorServiceException {
        try {
            return factory.getAssumptionDao().addAssumption(assumption);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }


    @Override
    public List<Assumption> getValidAssumptions(boolean valid, int start, int count) throws TotalizatorServiceException {
        if ((start - 1) * count < 0) {
            start = 1;
        }
        int index = (start - 1) * count;
        try {
            AssumptionDao assumptionDao = factory.getAssumptionDao();
            List<Assumption> assumptions = assumptionDao.getValidAssumption(valid, index, count);
            return assumptions;
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public int getCountValidAssumption(boolean valid) throws TotalizatorServiceException {
        try {
            AssumptionDao assumptionDao = factory.getAssumptionDao();
            int count = assumptionDao.getCountValidAssumption(valid);
            return count;
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
