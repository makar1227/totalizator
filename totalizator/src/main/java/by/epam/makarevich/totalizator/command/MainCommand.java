package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Assumption;
import by.epam.makarevich.totalizator.service.interfaces.AssumptionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MainCommand extends Command {
    @Override
    public Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) {

        int totalPages = 0;

        String pageParameter = req.getParameter("page");
        int pageIndex = 1;
        int count = 5;
        if (pageParameter != null) {
            try {
                pageIndex = Integer.parseInt(pageParameter);
            } catch (NumberFormatException e) {
                logger.trace("Invalid page parameter passed (" + pageParameter + ")");
            }
        }
        try {
            AssumptionService assumptionService = factory.getService(AssumptionService.class);
            if (totalPages == 0) {
                totalPages = assumptionService.getCountValidAssumption(true);
            }
            totalPages = (int) Math.ceil(((double) totalPages) / count);

            List<Assumption> assumptions = assumptionService.getValidAssumptions(true, pageIndex, count);

            req.setAttribute("assumptions", assumptions);
            req.setAttribute("totalPages", totalPages);
            req.setAttribute("page", pageIndex);
            return new Forward("index.jsp");
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }
    }
}
