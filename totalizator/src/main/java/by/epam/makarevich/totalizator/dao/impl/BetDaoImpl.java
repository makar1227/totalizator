package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.BetConstant;
import by.epam.makarevich.totalizator.dao.constants.MatchConstant;
import by.epam.makarevich.totalizator.dao.constants.TeamConstant;
import by.epam.makarevich.totalizator.dao.interfaces.BetDao;
import by.epam.makarevich.totalizator.dao.interfaces.UserDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Bet dao.
 */

public class BetDaoImpl implements BetDao {

    private static Logger logger = Logger.getLogger(BetDaoImpl.class);

    private static final String QUERY_INSERT = "INSERT INTO totalizator.bet (user_id, match_id, result_common, " +
            "score_first_team, score_second_team, amount) VALUES (?,?,?,?,?,?)";
    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM totalizator.bet WHERE id=?";
    private static final String QUERY_SELECT_BY_MATCH_ID = "SELECT * FROM totalizator.bet WHERE match_id=?";
    private static final String QUERY_SELECT_BY_USER_ID = "SELECT  B.id, M.match_date, \n" +
            "T1.sport, T1.team_name T1Name, T2.team_name T2Name, \n" +
            "B.user_id, B.result_common, B.score_first_team, B.score_second_team, B.amount, M.match_status, M.result_first_team, M.result_second_team \n" +
            "FROM totalizator.bet B \n" +
            "INNER join totalizator.match M\n" +
            "ON B.match_id=M.id INNER JOIN totalizator.team T1 ON M.first_team_id=T1.id\n" +
            "INNER JOIN totalizator.team T2 ON M.second_team_id=T2.id\n" +
            "WHERE B.user_id=? ORDER BY M.match_date DESC  LIMIT ?, ?";
    private static final String QUERY_COUNT_USER_BETS = "SELECT COUNT(*) FROM totalizator.bet WHERE user_id=?";
    private static final String QUERY_UPDATE = "UPDATE totalizator.bet SET result_common=?,score_first_team=?, score_second_team=?, amount=? WHERE id=?";
    private static final String QUERY_DELETE_BY_ID = "DELETE FROM totalizator.bet WHERE id=?";


    private List<Bet> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Bet> bets = new LinkedList<>();
        try {
            while (result.next()) {
                Bet bet = new Bet();
                User user = new User();
                Match match = new Match();
                bet.setId(result.getInt(BetConstant.ID));
                user.setId(result.getInt(BetConstant.USER_ID));
                bet.setUser(user);
                match.setId(result.getInt(BetConstant.MATCH_ID));
                bet.setMatch(match);
                Result matchResult = Result.getByName(result.getString(BetConstant.RESULT));
                bet.setResult(matchResult);
                bet.setFirstTeamScore(result.getInt(BetConstant.FIRST_TEAM_SCORE));
                bet.setSecondTeamScore(result.getInt(BetConstant.SECOND_TEAM_SCORE));
                bet.setCashSum(result.getDouble(BetConstant.CASH));
                bets.add(bet);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Bet resultSet.", e);
        }

        return bets;
    }


    private void prepareStatementForInsert(PreparedStatement statement, Bet bet) throws TotalizatorSqlException {
        try {
            statement.setInt(1, bet.getUser().getId());
            statement.setInt(2, bet.getMatch().getId());
            statement.setInt(3, bet.getResult().getId(bet.getResult()));
            statement.setInt(4, bet.getFirstTeamScore());
            statement.setInt(5, bet.getSecondTeamScore());
            statement.setDouble(6, bet.getCashSum());

        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for insert Bet.", e);

        }
    }

    private void prepareStatementForUpdate(PreparedStatement statement, Bet bet) throws TotalizatorSqlException {
        try {
            statement.setInt(1, bet.getResult().getId(bet.getResult()));
            statement.setInt(2, bet.getFirstTeamScore());
            statement.setInt(3, bet.getSecondTeamScore());
            statement.setDouble(4, bet.getCashSum());
            statement.setInt(5, bet.getId());

        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for update Bet.", e);

        }
    }


    @Override
    public List<Bet> getUserBets(int userId, int start, int count) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_USER_ID;
        List<Bet> bets = new ArrayList<>();
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, userId);
            statement.setInt(2, start);
            statement.setInt(3, count);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Bet bet = new Bet();
                    Match match = new Match();
                    Team team1 = new Team();
                    Team team2 = new Team();

                    bet.setId(result.getInt(BetConstant.ID));
                    match.setDate(result.getTimestamp(MatchConstant.DATE));
                    match.setfResult(result.getInt(MatchConstant.FIRST_TEAM_SCORE));
                    match.setsResult(result.getInt(MatchConstant.SECOND_TEAM_SCORE));
                    bet.setMatch(match);

                    team1.setName(result.getString(MatchConstant.FIRST_TEAM_NAME));
                    Sport sport = Sport.getByName(result.getString(TeamConstant.SPORT));
                    team1.setSport(sport);
                    team2.setName(result.getString(MatchConstant.SECOND_TEAM_NAME));
                    bet.getMatch().setFirstTeam(team1);
                    bet.getMatch().setSecondTeam(team2);
                    bet.getMatch().setMatchStatus(result.getBoolean(MatchConstant.MATCH_STATUS));
                    bet.setResult(Result.getByName(result.getString(BetConstant.RESULT)));
                    if (result.getInt(BetConstant.FIRST_TEAM_SCORE) != -1 && result.getInt(BetConstant.SECOND_TEAM_SCORE) != -1) {
                        bet.setFirstTeamScore(result.getInt(BetConstant.FIRST_TEAM_SCORE));
                        bet.setSecondTeamScore(result.getInt(BetConstant.SECOND_TEAM_SCORE));
                    }
                    bet.setCashSum(result.getDouble(BetConstant.CASH));

                    bets.add(bet);
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting user's bets.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return bets;
    }

    @Override
    public int getCountUserBets(int userId) throws TotalizatorSqlException {
        String sql = QUERY_COUNT_USER_BETS;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        int count = 0;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, userId);
            try (ResultSet result = statement.executeQuery()) {
                result.next();
                count = result.getInt(1);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with counting user's bets.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }

        return count;
    }

    @Override
    public List<Bet> getMatchBets(int matchId) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_MATCH_ID;
        List<Bet> bets;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, matchId);
            try (ResultSet result = statement.executeQuery()) {
                bets = parseResultSet(result);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting user's bets.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return bets;
    }

    @Override
    public Bet getBetById(int betId) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_ID;
        List<Bet> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, betId);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting bet by id ", e);
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public boolean addBet(Bet bet, User user) throws TotalizatorSqlException {
        String sql = QUERY_INSERT;
        UserDao userDao=new UserDaoImpl();

        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, bet);

            connection.setAutoCommit(false);
            int count = statement.executeUpdate();
            userDao.editUserCash(connection, user);
            connection.commit();

            if (count == 1) {
                logger.info("Bet was successfully created!");
            } else {
                logger.error("Bet wasn't added ");
                throw new TotalizatorSqlException("Bet wasn't added ");
            }
        } catch (SQLException e) {
            try{
                connection.rollback();
            }catch (SQLException e1){
                throw new TotalizatorSqlException("Cannot insert bet object in DB, rollback operation failed", e);
            }
            throw new TotalizatorSqlException("Cannot insert bet object in DB", e);
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return true;
    }

    @Override
    public void deleteBetById(int betId) throws TotalizatorSqlException {
        String sql = QUERY_DELETE_BY_ID;
        UserDao userDao=new UserDaoImpl();
        BetDao betDao=new BetDaoImpl();

        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, betId);

            Bet bet=betDao.getBetById(betId);
            User user=userDao.getUserById(bet.getUser().getId());
            user.setCashAmount(user.getCashAmount() + bet.getCashSum());

            connection.setAutoCommit(false);
            int result = statement.executeUpdate();
            userDao.editUserCash(connection, user);
            connection.commit();

            if (result != 1) {
                throw new TotalizatorSqlException("Problem with deleting bet with id = " + betId);
            }
        } catch (SQLException e) {
            try{
                connection.rollback();
            }catch (SQLException e1){
                throw new TotalizatorSqlException("Cannot delete bet object from DB, rollback operation failed", e);
            }
            throw new TotalizatorSqlException("Problem with deleting bet with id = " + betId, e);
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    @Override
    public void editBet(Bet bet, User user) throws TotalizatorSqlException {
        String sql = QUERY_UPDATE;
        UserDao userDao = new UserDaoImpl();

        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(statement, bet);

            connection.setAutoCommit(false);
            int count = statement.executeUpdate();
            userDao.editUserCash(connection, user);
            connection.commit();

            if (count == 1) {
                logger.info("Bet with id = " + bet.getId() + " was successfully updated!");
            } else {
                logger.error("Bet with id = " + bet.getId() + " wasn't updated.");
                throw new TotalizatorSqlException("Bet with id = " + bet.getId() + " wasn't updated.");
            }

        } catch (SQLException e) {
            try{
                connection.rollback();
            }catch (SQLException e1){
                throw new TotalizatorSqlException("Cannot edit bet object from DB, rollback operation failed", e);
            }
            logger.error("Can not edit bet with id = " + bet.getId());
        }finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    public BetDaoImpl() {
        super();
    }
}
