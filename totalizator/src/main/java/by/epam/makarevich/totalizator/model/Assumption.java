package by.epam.makarevich.totalizator.model;

import java.io.Serializable;


/**
 * The type Assumption.
 */
public class Assumption extends Entity implements Serializable {

    private static final long serialVersionUID = 4990692451193217383L;

    private Broker broker;
    private Match match;
    private double fWin;
    private double draw;
    private double sWin;


    /**
     * Gets broker.
     *
     * @return the broker
     */
    public Broker getBroker() {
        return broker;
    }

    /**
     * Sets broker.
     *
     * @param broker the broker
     */
    public void setBroker(Broker broker) {
        this.broker = broker;
    }

    /**
     * Gets match.
     *
     * @return the match
     */
    public Match getMatch() {
        return match;
    }

    /**
     * Sets match.
     *
     * @param match the match
     */
    public void setMatch(Match match) {
        this.match = match;
    }

    /**
     * Gets win.
     *
     * @return the win
     */
    public double getfWin() {
        return fWin;
    }

    /**
     * Sets win.
     *
     * @param fWin the f win
     */
    public void setfWin(double fWin) {
        this.fWin = fWin;
    }

    /**
     * Gets draw.
     *
     * @return the draw
     */
    public double getDraw() {
        return draw;
    }

    /**
     * Sets draw.
     *
     * @param draw the draw
     */
    public void setDraw(double draw) {
        this.draw = draw;
    }

    /**
     * Gets win.
     *
     * @return the win
     */
    public double getsWin() {
        return sWin;
    }

    /**
     * Sets win.
     *
     * @param sWin the s win
     */
    public void setsWin(double sWin) {
        this.sWin = sWin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Assumption that = (Assumption) o;

        if (Double.compare(that.fWin, fWin) != 0) return false;
        if (Double.compare(that.draw, draw) != 0) return false;
        if (Double.compare(that.sWin, sWin) != 0) return false;
        if (broker != null ? !broker.equals(that.broker) : that.broker != null) return false;
        return match != null ? match.equals(that.match) : that.match == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (broker != null ? broker.hashCode() : 0);
        result = 31 * result + (match != null ? match.hashCode() : 0);
        temp = Double.doubleToLongBits(fWin);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(draw);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sWin);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Assumption{");
        sb.append("broker=").append(broker.getId());
        sb.append(", match=").append(match.getId());
        sb.append(", fWin=").append(fWin);
        sb.append(", draw=").append(draw);
        sb.append(", sWin=").append(sWin);
        sb.append('}');
        return sb.toString();
    }
}
