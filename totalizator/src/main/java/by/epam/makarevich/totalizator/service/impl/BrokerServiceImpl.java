package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Broker;
import by.epam.makarevich.totalizator.service.interfaces.BrokerService;


/**
 * The type Broker service.
 */
public class BrokerServiceImpl extends ServiceImpl implements BrokerService {
    @Override
    public Broker getBrokerById(int id) throws TotalizatorServiceException {
        try {
            return factory.getBrokerDao().getBrokerById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public Broker getBrokerByLogin(String login) throws TotalizatorServiceException {
        try {
            return factory.getBrokerDao().getBrokerByLogin(login);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
