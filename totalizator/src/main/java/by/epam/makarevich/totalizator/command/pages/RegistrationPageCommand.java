package by.epam.makarevich.totalizator.command.pages;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RegistrationPageCommand extends Command {

    @Override
    public Command.Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        Authorities authorities = (Authorities) session.getAttribute("authorizedUser");
        if (authorities == null) {
            return new Forward("/registration.jsp");
        }
        return new Forward("/",true);
    }
}
