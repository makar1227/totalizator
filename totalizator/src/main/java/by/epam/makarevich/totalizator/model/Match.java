package by.epam.makarevich.totalizator.model;

import java.io.Serializable;
import java.util.Date;


/**
 * The type Match.
 */
public class Match extends Entity implements Serializable {
    private static final long serialVersionUID = -5160201796657553170L;

    private Team firstTeam;
    private Team secondTeam;
    private Date date;
    private Result result;
    private int fResult;
    private int sResult;
    private boolean matchStatus;

    /**
     * Instantiates a new Match.
     */
    public Match() {
        fResult=-1;
        sResult=-1;
        matchStatus=false;
    }

    /**
     * Gets first team.
     *
     * @return the first team
     */
    public Team getFirstTeam() {
        return firstTeam;
    }


    /**
     * Sets first team.
     *
     * @param firstTeam the first team
     */
    public void setFirstTeam(Team firstTeam) {
        this.firstTeam = firstTeam;
    }

    /**
     * Gets second team.
     *
     * @return the second team
     */
    public Team getSecondTeam() {
        return secondTeam;
    }

    /**
     * Sets second team.
     *
     * @param secondTeam the second team
     */
    public void setSecondTeam(Team secondTeam) {
        this.secondTeam = secondTeam;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets f result.
     *
     * @return the f result
     */
    public int getfResult() {
        return fResult;
    }

    /**
     * Sets result.
     *
     * @param fResult the f result
     */
    public void setfResult(int fResult) {
        this.fResult = fResult;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public int getsResult() {
        return sResult;
    }

    /**
     * Sets result.
     *
     * @param sResult the s result
     */
    public void setsResult(int sResult) {
        this.sResult = sResult;
    }

    /**
     * Is match status boolean.
     *
     * @return the boolean
     */
    public boolean isMatchStatus() {
        return matchStatus;
    }

    /**
     * Sets match status.
     *
     * @param matchStatus the match status
     */
    public void setMatchStatus(boolean matchStatus) {
        this.matchStatus = matchStatus;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Match match = (Match) o;

        if (fResult != match.fResult) return false;
        if (sResult != match.sResult) return false;
        if (matchStatus != match.matchStatus) return false;
        if (firstTeam != null ? !firstTeam.equals(match.firstTeam) : match.firstTeam != null) return false;
        if (secondTeam != null ? !secondTeam.equals(match.secondTeam) : match.secondTeam != null) return false;
        if (date != null ? !date.equals(match.date) : match.date != null) return false;
        return result == match.result;
    }

    @Override
    public int hashCode() {
        int result1 = super.hashCode();
        result1 = 31 * result1 + (firstTeam != null ? firstTeam.hashCode() : 0);
        result1 = 31 * result1 + (secondTeam != null ? secondTeam.hashCode() : 0);
        result1 = 31 * result1 + (date != null ? date.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + fResult;
        result1 = 31 * result1 + sResult;
        result1 = 31 * result1 + (matchStatus ? 1 : 0);
        return result1;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Match{");
        sb.append("firstTeam=").append(firstTeam.getId());
        sb.append(", secondTeam=").append(secondTeam.getId());
        sb.append(", date=").append(date);
        sb.append(", result=").append(result);
        sb.append(", fResult=").append(fResult);
        sb.append(", sResult=").append(sResult);
        sb.append(", matchStatus=").append(matchStatus);
        sb.append('}');
        return sb.toString();
    }
}
