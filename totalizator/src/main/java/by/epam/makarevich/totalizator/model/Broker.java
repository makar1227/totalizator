package by.epam.makarevich.totalizator.model;

import java.io.Serializable;


/**
 * The type Broker.
 */
public class Broker extends Entity implements Serializable {
    private static final long serialVersionUID = 438155602886880396L;

    private String login;
    private String password;

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Broker)) return false;
        if (!super.equals(o)) return false;

        Broker broker = (Broker) o;

        if (login != null ? !login.equals(broker.login) : broker.login != null) return false;
        return password != null ? password.equals(broker.password) : broker.password == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Broker{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
