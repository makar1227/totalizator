package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Authorities;

import java.sql.Connection;


/**
 * The interface Authorities dao.
 */
public interface AuthoritiesDao {

    Authorities getAuthorityByLogin(String login) throws TotalizatorSqlException;

    Authorities getAuthorityById(int id) throws TotalizatorSqlException;

    boolean addAuthority(Authorities authorities) throws TotalizatorSqlException;

    boolean addAuthority(Connection connection, Authorities authorities) throws TotalizatorSqlException;

    void editAuthority(Authorities authorities) throws TotalizatorSqlException;

    void editAuthority(Connection connection, Authorities authorities) throws TotalizatorSqlException;

}

