package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.dao.impl.MySqlDaoFactory;
import by.epam.makarevich.totalizator.service.interfaces.Service;
/**
 * The type Service.
 */
public class ServiceImpl implements Service {
    /**
     * The Factory.
     */
    protected MySqlDaoFactory factory;

    /**
     * Sets dao factory.
     *
     * @param factory the factory
     */
    public void setDaoFactory(MySqlDaoFactory factory) {
        this.factory = factory;
    }
}
