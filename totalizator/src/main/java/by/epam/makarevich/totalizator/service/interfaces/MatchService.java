package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;

import java.util.List;

public interface MatchService extends Service {

    Match getMatchById(int id) throws TotalizatorServiceException;

    List<Match> getMatchesByStatus(boolean isValid, int start, int count) throws TotalizatorServiceException;

    int getCountMatchesByStatus(boolean isValid) throws TotalizatorServiceException;

    boolean addMatch(Match match) throws TotalizatorServiceException;

    void editMatch(Match match) throws TotalizatorServiceException;

}
