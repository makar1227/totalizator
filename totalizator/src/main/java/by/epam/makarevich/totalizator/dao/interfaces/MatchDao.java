package by.epam.makarevich.totalizator.dao.interfaces;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Assumption;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.Match;

import java.sql.Connection;
import java.util.List;

public interface MatchDao {
    Match getMatchById(int id) throws TotalizatorSqlException;

    boolean addMatch(Match match) throws TotalizatorSqlException;

    void editMatch(Match match, Assumption assumption, List<Bet> bets) throws TotalizatorSqlException;

    List<Match> getMatchesByStatus(boolean valid, int start, int count) throws TotalizatorSqlException;

    void editMatchStatus(Match match) throws TotalizatorSqlException;

    void editMatchStatus(Connection connection, Match match) throws TotalizatorSqlException;


    int getCountMatchesByStatus(boolean valid) throws TotalizatorSqlException;


}
