package by.epam.makarevich.totalizator.dao.pool;

import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
//import javax.sql.DataSource;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.sql.Connection;
import java.sql.SQLException;


/**
 * The type Connection pool.
 */
public class ConnectionPool {

    private static Logger logger = Logger.getLogger(ConnectionPool.class);
    private static DataSource dataSource;

    private ConnectionPool() {
        try {
            InitialContext initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("java:comp/env/jdbc/root");
        } catch (NamingException e) {
            logger.error("Error in pool connections");
        }
    }

    private static class ConnectionPoolSingl {
        /**
         * The constant pool.
         */
        public static final ConnectionPool pool = new ConnectionPool();
    }

    /**
     * Get pool connection pool.
     *
     * @return the connection pool
     */
    public static ConnectionPool getPool() {
        return ConnectionPoolSingl.pool;
    }

    /**
     * Gets connection.
     *
     * @return the connection
     * @throws TotalizatorSqlException the totalizator sql exception
     */
    public Connection getConnection() throws TotalizatorSqlException {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Cannot get a connection", e);
        }
    }

    /**
     * Free connection.
     *
     * @param connection the connection
     * @throws TotalizatorSqlException the totalizator sql exception
     */
    public void freeConnection(Connection connection) throws TotalizatorSqlException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Cannot free connection", e);
        }
    }

}
//public class ConnectionPool {

//    private static Logger logger = Logger.getLogger(ConnectionPool.class);
//    private static DataSource dataSource;
//    private org.apache.tomcat.jdbc.pool.ConnectionPool pool;
//
//    private ConnectionPool() {
//        try {
//            PoolProperties p = new PoolProperties();
//            p.setUrl("jdbc:mysql://localhost:3306/totalizator?useSSL=false&amp;zeroDateTimeBehavior=convertToNull&amp;autoReconnect=true\"\n");
//            p.setDriverClassName("com.mysql.jdbc.Driver");
//            p.setUsername("root");
//            p.setPassword("unreal2k3");
//            p.setJmxEnabled(true);
//            p.setTestWhileIdle(false);
//            p.setTestOnBorrow(true);
//            p.setTestOnReturn(false);
//            p.setValidationInterval(30000);
//            p.setTimeBetweenEvictionRunsMillis(30000);
//            p.setMaxActive(10);
//            p.setInitialSize(10);
//            p.setMaxWait(10000);
//            p.setRemoveAbandonedTimeout(60);
//            p.setMinEvictableIdleTimeMillis(30000);
//            p.setMinIdle(10);
//            p.setLogAbandoned(true);
//            p.setRemoveAbandoned(true);
//            p.setJdbcInterceptors(
//                    "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
//                            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
//            dataSource = new DataSource();
//            dataSource.setPoolProperties(p);
//
//            pool = dataSource.createPool();
//            System.out.println("size = "+pool.getIdle());
//            for (int i = 0; i < 11; i++) {
//                Connection con=pool.getConnection();
//                System.out.println(con.hashCode());
//                System.out.println("idle = "+pool.getIdle());
//                System.out.println("active = "+pool.getActive());
//                if(pool.getActive()==9){
//                    System.out.println("con close "+con.hashCode());
//                    con.close();
//                }
//            }
//
//            System.out.println(dataSource.getSize());
//
//
////            Context initialContext = new InitialContext();
////            dataSource = (DataSource) initialContext.lookup("java:comp/env/jdbc/root");
////           System.out.println("active connections = "+dataSource);
//        } catch (Exception e) {//Naming
//            logger.error("Error in pool connections " + e.getMessage());
//        }
//    }
//
//    private static class ConnectionPoolSingl {
//        public static final ConnectionPool pool = new ConnectionPool();
//    }
//
//    public static ConnectionPool getPool() {
//
//        System.out.println("CON POOL");
//        return ConnectionPoolSingl.pool;
//    }
//
//    public Connection getConnection() throws TotalizatorSqlException {
//        try {
//            Connection connection = dataSource.getConnection();
//            Connection actual = ((javax.sql.PooledConnection)connection).getConnection();
////            Connection connection=pool.getConnection();
////            Connection actual = ((javax.sql.PooledConnection)connection).getConnection();
//            System.out.println("actual  "+actual.hashCode());
//
//            System.out.println("open connection " + connection.hashCode());
////            Connection actual = ((javax.sql.PooledConnection)connection).getConnection();
////            System.out.println(actual.getCatalog());
////            System.out.println("open connection "+actual.hashCode());
//            return connection;
//        } catch (SQLException e) {
//            throw new TotalizatorSqlException("Cannot get a connection", e);
//        }
//    }
//
//    public void freeConnection(Connection connection) throws TotalizatorSqlException {
//        try {
//            System.out.println("close connection " + connection.hashCode());
//            connection.close();
//        } catch (SQLException e) {
//            throw new TotalizatorSqlException("Cannot free connection", e);
//        }
//    }

//}

