package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Team;

import java.util.List;


public interface TeamService extends Service {
    Team getTeamById(int id) throws TotalizatorServiceException;

    Team getTeamByName(String name) throws TotalizatorServiceException;

    List<Team> getAllTeams() throws TotalizatorServiceException;

}
