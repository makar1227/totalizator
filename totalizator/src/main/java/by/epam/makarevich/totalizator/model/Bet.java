package by.epam.makarevich.totalizator.model;

import java.io.Serializable;


/**
 * The type Bet.
 */
public class Bet extends Entity implements Serializable {

    private static final long serialVersionUID = -3653230084127489905L;

    private User user;
    private Match match;
    private Result result;
    private int firstTeamScore;
    private int secondTeamScore;
    private double cashSum;
    private double winAmount;
    private boolean paid;

    /**
     * Instantiates a new Bet.
     */
    public Bet() {
        firstTeamScore =-1;
        secondTeamScore =-1;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets match.
     *
     * @return the match
     */
    public Match getMatch() {
        return match;
    }

    /**
     * Sets match.
     *
     * @param match the match
     */
    public void setMatch(Match match) {
        this.match = match;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    /**
     * Gets first team score.
     *
     * @return the first team score
     */
    public int getFirstTeamScore() {
        return firstTeamScore;
    }

    /**
     * Sets first team score.
     *
     * @param firstTeamScore the first team score
     */
    public void setFirstTeamScore(int firstTeamScore) {
        this.firstTeamScore = firstTeamScore;
    }

    /**
     * Gets second team score.
     *
     * @return the second team score
     */
    public int getSecondTeamScore() {
        return secondTeamScore;
    }

    /**
     * Sets second team score.
     *
     * @param secondTeamScore the second team score
     */
    public void setSecondTeamScore(int secondTeamScore) {
        this.secondTeamScore = secondTeamScore;
    }

    /**
     * Gets cash sum.
     *
     * @return the cash sum
     */
    public double getCashSum() {
        return cashSum;
    }

    /**
     * Sets cash sum.
     *
     * @param cashSum the cash sum
     */
    public void setCashSum(double cashSum) {
        this.cashSum = cashSum;
    }

    /**
     * Gets win amount.
     *
     * @return the win amount
     */
    public double getWinAmount() {
        return winAmount;
    }

    /**
     * Sets win amount.
     *
     * @param winAmount the win amount
     */
    public void setWinAmount(double winAmount) {
        this.winAmount = winAmount;
    }

    /**
     * Is paid boolean.
     *
     * @return the boolean
     */
    public boolean isPaid() {
        return paid;
    }

    /**
     * Sets paid.
     *
     * @param paid the paid
     */
    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bet bet = (Bet) o;

        if (firstTeamScore != bet.firstTeamScore) return false;
        if (secondTeamScore != bet.secondTeamScore) return false;
        if (Double.compare(bet.cashSum, cashSum) != 0) return false;
        if (Double.compare(bet.winAmount, winAmount) != 0) return false;
        if (paid != bet.paid) return false;
        if (user != null ? !user.equals(bet.user) : bet.user != null) return false;
        if (match != null ? !match.equals(bet.match) : bet.match != null) return false;
        return result == bet.result;
    }

    @Override
    public int hashCode() {
        int result1 = super.hashCode();
        long temp;
        result1 = 31 * result1 + (user != null ? user.hashCode() : 0);
        result1 = 31 * result1 + (match != null ? match.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + firstTeamScore;
        result1 = 31 * result1 + secondTeamScore;
        temp = Double.doubleToLongBits(cashSum);
        result1 = 31 * result1 + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(winAmount);
        result1 = 31 * result1 + (int) (temp ^ (temp >>> 32));
        result1 = 31 * result1 + (paid ? 1 : 0);
        return result1;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Bet{");
        sb.append("user=").append(user.getId());
        sb.append(", match=").append(match.getId());
        sb.append(", result=").append(result);
        sb.append(", firstTeamScore=").append(firstTeamScore);
        sb.append(", secondTeamScore=").append(secondTeamScore);
        sb.append(", cashSum=").append(cashSum);
        sb.append(", winAmount=").append(winAmount);
        sb.append(", paid=").append(paid);
        sb.append('}');
        return sb.toString();
    }
}
