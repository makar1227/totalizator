package by.epam.makarevich.totalizator.model;


/**
 * The enum Role.
 */
public enum Role {
    /**
     * Admin role.
     */
    ADMIN("ROLE_ADMIN"),
    /**
     * User role.
     */
    USER("ROLE_USER"),
    /**
     * Broker role.
     */
    BROKER("ROLE_BROKER");

    private String role;

    private Role(String role) {
        this.role = role;
    }

    /**
     * Get by id int.
     *
     * @param role the role
     * @return the int
     */
    public static int getById(Role role){
        switch (role) {
            case ADMIN:
                return 1;
            case USER:
                return 2 ;
            case BROKER:
                return 3;
            default:
                return 0;
        }
    }

    /**
     * Get by name role.
     *
     * @param name the name
     * @return the role
     */
    public static Role getByName(String name){
        switch (name){
            case "ROLE_ADMIN":
                return ADMIN;
            case "ROLE_BROKER":
                return BROKER;
            default:
                return USER;
        }
    }
}
