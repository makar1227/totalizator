package by.epam.makarevich.totalizator.exceptions.dao;


public class TotalizatorSqlException extends Exception {
    public TotalizatorSqlException() {
        super();
    }

    public TotalizatorSqlException(String message) {
        super(message);
    }

    public TotalizatorSqlException(String message, Throwable cause) {
        super(message, cause);
    }

    public TotalizatorSqlException(Throwable cause) {
        super(cause);
    }
}
