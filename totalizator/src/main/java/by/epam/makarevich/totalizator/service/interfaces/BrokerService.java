package by.epam.makarevich.totalizator.service.interfaces;

import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Broker;

public interface BrokerService extends Service {

    Broker getBrokerByLogin(String login) throws TotalizatorServiceException;

    Broker getBrokerById(int id) throws TotalizatorServiceException;

}
