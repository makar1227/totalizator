package by.epam.makarevich.totalizator.command.pages;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Bet;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.interfaces.BetService;
import by.epam.makarevich.totalizator.service.interfaces.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


public class ProfilePageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        int totalPages = 0;

        String pageParameter = req.getParameter("page");
        int pageIndex = 1;
        int count = 5;
        if (pageParameter != null) {
            try {
                pageIndex = Integer.parseInt(pageParameter);
            } catch (NumberFormatException e) {
                logger.trace("Invalid page parameter passed (" + pageParameter + ")");
            }
        }

        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("authorizedUser") != null) {
            Authorities authorities = (Authorities) session.getAttribute("authorizedUser");

            try {
                UserService userService = factory.getService(UserService.class);
                User user = userService.getUserByLogin(authorities.getLogin());
                BetService betService = factory.getService(BetService.class);

                if (totalPages == 0) {
                    totalPages = betService.getCountUserBets(user.getId());
                }
                totalPages = (int) Math.ceil(((double) totalPages) / count);

                List<Bet> bets = betService.getUserBets(user.getId(), pageIndex, count);

                req.setAttribute("bets", bets);
                req.setAttribute("user", user);
                req.setAttribute("totalPages", totalPages);
                req.setAttribute("page", pageIndex);

                return new Forward("/profile.jsp");
            } catch (TotalizatorServiceException e) {
                logger.error(e.getMessage(), e);
                return new Forward("/error.jsp");
            }
        } else return new Forward("/login.jsp");
    }
}