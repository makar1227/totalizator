package by.epam.makarevich.totalizator.dao.impl;


import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;

/**
 * The type My sql dao factory.
 */
public class MySqlDaoFactory {

    private static final MySqlDaoFactory myFactory = new MySqlDaoFactory();


    //    TODO different pool connection tomcat and my
//    private ConnectionPool pool = ConnectionPool.getPool();

    private MyConnectionPool pool = MyConnectionPool.getInstance();


    /**
     * Instantiates a new My sql dao factory.
     */
    private MySqlDaoFactory() {
    }

    public UserDaoImpl getUserDao() {
        return new UserDaoImpl();
    }

    public BetDaoImpl getBetDao() {
        return new BetDaoImpl();
    }

    public BrokerDaoImpl getBrokerDao() {
        return new BrokerDaoImpl();
    }

    public MatchDaoImpl getMatchDao() {
        return new MatchDaoImpl();
    }

    public AssumptionDaoImpl getAssumptionDao() {
        return new AssumptionDaoImpl();
    }

    public AuthoritiesDaoImpl getAuthoritiesDao() {
        return new AuthoritiesDaoImpl();
    }

    public TeamDaoImpl getTeamDao() {
        return new TeamDaoImpl();
    }


    public static MySqlDaoFactory getMyFactory() {
        return myFactory;
    }
}
