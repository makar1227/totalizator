package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.AuthoritiesConstant;
import by.epam.makarevich.totalizator.dao.interfaces.AuthoritiesDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.Role;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Authorities dao.
 */

public class AuthoritiesDaoImpl implements AuthoritiesDao {

    private static Logger logger = Logger.getLogger(AuthoritiesDaoImpl.class);


    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM totalizator.authorities WHERE id=?";
    private static final String QUERY_SELECT_BY_LOGIN = "SELECT * FROM totalizator.authorities WHERE login=?";
    private static final String QUERY_INSERT = "INSERT INTO totalizator.authorities (login, password) VALUES (?,?)";
    private static final String QUERY_UPDATE = "UPDATE totalizator.authorities SET  password=? WHERE login=?";


    private List<Authorities> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<Authorities> authorities = new ArrayList<>();
        try {
            while (result.next()) {
                Authorities authority = new Authorities();
                authority.setId(result.getInt(AuthoritiesConstant.ID));
                authority.setLogin(result.getString(AuthoritiesConstant.LOGIN));
                Role role = Role.getByName(result.getString(AuthoritiesConstant.ROLE));
                authority.setRole(role);
                authority.setPassword(result.getString(AuthoritiesConstant.PASSWORD));
                authorities.add(authority);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing Authorities resultSet.", e);
        }
        return authorities;
    }

    private void prepareStatementForInsert(PreparedStatement statement, Authorities authorities) throws TotalizatorSqlException {
        try {
            statement.setString(1, authorities.getLogin());
            statement.setString(2, authorities.getPassword());
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for insert Authority.", e);
        }
    }

    private void prepareStatementForUpdate(PreparedStatement statement, Authorities authorities) throws TotalizatorSqlException {
        try {
            statement.setString(1, authorities.getPassword());
            statement.setString(2, authorities.getLogin());
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for update Authority.", e);
        }
    }

    @Override
    public Authorities getAuthorityByLogin(String login) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_LOGIN;
        List<Authorities> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Authority by login.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public Authorities getAuthorityById(int id) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_ID;
        List<Authorities> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting Authority by id.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public boolean addAuthority(Authorities authorities) throws TotalizatorSqlException {
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try {
            addAuthority(connection, authorities);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return true;
    }

    @Override
    public boolean addAuthority(Connection connection, Authorities authorities) throws TotalizatorSqlException {
        String sql = QUERY_INSERT;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, authorities);
            int count = statement.executeUpdate();
            if (count == 1) {
                logger.info("Authorities was successfully created!");
            } else {
                logger.error("Authorities wasn't added ");
                throw new TotalizatorSqlException("Authorities wasn't added ");
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Cannot insert authorities object in DB", e);
        }
        return true;
    }

    @Override
    public void editAuthority(Authorities authorities) throws TotalizatorSqlException {

        Connection connection = MyConnectionPool.getInstance().getConnection();
        try {
            editAuthority(connection, authorities);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    @Override
    public void editAuthority(Connection connection, Authorities authorities) throws TotalizatorSqlException {
        String sql = QUERY_UPDATE;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(statement, authorities);
            int count = statement.executeUpdate();
            if (count == 1) {
                logger.info("Authorities with id = " + authorities.getId() + " was successfully updated!");
            } else {
                logger.error("Authorities with id = " + authorities.getId() + " wasn't updated.");
                throw new TotalizatorSqlException("Authorities with id = " + authorities.getId() + " wasn't updated.");
            }

        } catch (SQLException e) {
            logger.error("Can not update authorities with id = " + authorities.getId());
        }
    }

    public AuthoritiesDaoImpl() {
        super();
    }

}
