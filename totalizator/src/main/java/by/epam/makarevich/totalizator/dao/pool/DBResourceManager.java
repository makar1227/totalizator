package by.epam.makarevich.totalizator.dao.pool;

import java.util.ResourceBundle;

/**
 * The type Db resource manager.
 */
public class DBResourceManager {
    private static final DBResourceManager instance = new DBResourceManager();

    public DBResourceManager() {}

    private ResourceBundle bundle = ResourceBundle.getBundle("db");

    public static DBResourceManager getInstance() {
        return instance;
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }

}
