package by.epam.makarevich.totalizator.dao.impl;

import by.epam.makarevich.totalizator.dao.constants.UserConstant;
import by.epam.makarevich.totalizator.dao.interfaces.AuthoritiesDao;
import by.epam.makarevich.totalizator.dao.interfaces.UserDao;
import by.epam.makarevich.totalizator.dao.pool.MyConnectionPool;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.model.Authorities;
import by.epam.makarevich.totalizator.model.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * The type User dao.
 */
public class UserDaoImpl implements UserDao {

    private static Logger logger = Logger.getRootLogger();

    private static final String QUERY_INSERT = "INSERT INTO totalizator.user (login, password, mail, phone, cash_amount, birth) VALUES (?,?,?,?,?,?)";
    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM totalizator.user WHERE id=?";
    private static final String QUERY_SELECT_BY_LOGIN = "SELECT * FROM totalizator.user WHERE login=?";
    private static final String QUERY_SELECT_BY_EMAIL = "SELECT * FROM totalizator.user WHERE mail=?";
    private static final String QUERY_UPDATE = "UPDATE totalizator.user SET  password=?, mail=?, phone=?, cash_amount=? WHERE id=?";
    private static final String QUERY_UPDATE_CASH = "UPDATE totalizator.user SET  cash_amount=? WHERE id=?";


    private List<User> parseResultSet(ResultSet result) throws TotalizatorSqlException {
        List<User> users = new LinkedList<>();
        try {
            while (result.next()) {
                User user = new User();
                user.setId(result.getInt(UserConstant.ID));
                user.setLogin(result.getString(UserConstant.LOGIN));
                user.setPassword(result.getString(UserConstant.PASSWORD));
                user.setPhone(result.getString(UserConstant.PHONE));
                user.setEmail(result.getString(UserConstant.EMAIL));;
                user.setBirth(result.getDate(UserConstant.BIRTH));
                user.setCashAmount(result.getDouble(UserConstant.CASH));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with parsing User resultSet.", e);
        }
        return users;
    }

    private void prepareStatementForInsert(PreparedStatement statement, User user) throws TotalizatorSqlException {
        try {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhone());
            statement.setDouble(5, user.getCashAmount());
            java.sql.Timestamp newDate = new java.sql.Timestamp(user.getBirth().getTime());
            statement.setTimestamp(6, newDate);

        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for insert User.", e);
        }
    }

    private void prepareStatementForUpdate(PreparedStatement statement, User user) throws TotalizatorSqlException {
        try {
            statement.setString(1, user.getPassword());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPhone());
            statement.setDouble(4, user.getCashAmount());
            statement.setInt(5, user.getId());
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for update User.", e);
        }
    }

    private void prepareStatementForCashUpdate(PreparedStatement statement, User user) throws TotalizatorSqlException {
        try {
            statement.setDouble(1, user.getCashAmount());
            statement.setInt(2, user.getId());
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with statement for update cash.", e);
        }
    }

    @Override
    public User getUserByEmail(String email) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_EMAIL;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        List<User> list;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with finding User by email.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public User getUserByLogin(String login) throws TotalizatorSqlException {
        String sql = QUERY_SELECT_BY_LOGIN;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        List<User> list;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with finding User by login.", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return list.iterator().next();
    }

    @Override
    public User getUserById(int id) throws TotalizatorSqlException {

        String sql = QUERY_SELECT_BY_ID;
        List<User> list;
        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = parseResultSet(resultSet);
                if (list == null || list.size() != 1) {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new TotalizatorSqlException("Problem with getting user by id ", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        User user=list.iterator().next();
        return user;
    }


    @Override
    public void editUser(User user) throws TotalizatorSqlException {
        String sql = QUERY_UPDATE;
        AuthoritiesDao authoritiesDao = new AuthoritiesDaoImpl();
        Authorities authorities = authoritiesDao.getAuthorityByLogin(user.getLogin());
        authorities.setPassword(user.getPassword());

        Connection connection = MyConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(statement, user);
            connection.setAutoCommit(false);
            int count = statement.executeUpdate();
            authoritiesDao.editAuthority(connection, authorities);
            connection.commit();
            if (count == 1) {
                logger.info("User with id = " + user.getId() + " was successfully updated!");
            } else {
                logger.error("User with id = " + user.getId() + " wasn't updated.");
                throw new TotalizatorSqlException("User with id = " + user.getId() + " wasn't updated.");
            }

        } catch (SQLException e) {
            logger.error("Can not update user with id = " + user.getId());
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new TotalizatorSqlException("Can not update user object in DB, rollback operation failed", e);
            }
            throw new TotalizatorSqlException("Can not insert " + user.getClass().getSimpleName() + " object in DB", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
    }

    @Override
    public void editUserCash(Connection connection, User user) throws TotalizatorSqlException {

        String sql = QUERY_UPDATE_CASH;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForCashUpdate(statement, user);

            int count = statement.executeUpdate();

            if (count == 1) {
                logger.info("User with id = " + user.getId() + " updated cash!");
            } else {
                logger.error("User with id = " + user.getId() + " didn't update cash.");
                throw new TotalizatorSqlException("User with id = " + user.getId() + " wasn't updated.");
            }

        } catch (SQLException e) {
            logger.error("Can not update user with id = " + user.getId());
            throw new TotalizatorSqlException("Can not insert " + user.getClass().getSimpleName() + " object in DB", e);
        }
    }

    @Override
    public boolean addUser(User user) throws TotalizatorSqlException {
        String sql = QUERY_INSERT;
        Authorities authorities = new Authorities();
        authorities.setLogin(user.getLogin());
        authorities.setPassword(user.getPassword());

        Connection connection = MyConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, user);

            connection.setAutoCommit(false);
            int count = statement.executeUpdate();
            AuthoritiesDao authoritiesDao = new AuthoritiesDaoImpl();
            authoritiesDao.addAuthority(connection, authorities);
            connection.commit();
            if (count == 1) {
                logger.info("User entity was successfully created!");
            } else {
                logger.error("User wasn't added");
                throw new TotalizatorSqlException("User wasn't added ");
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new TotalizatorSqlException("Can not insert user object in DB, rollback operation", e);
            }
            throw new TotalizatorSqlException("Can not insert " + user.getClass().getSimpleName() + " object in DB", e);
        } finally {
            if (connection != null) {
                MyConnectionPool.getInstance().freeConnection(connection);
            }
        }
        return true;
    }

    public UserDaoImpl() {
        super();
    }
}
