package by.epam.makarevich.totalizator.command.user;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BetCommand extends Command {

    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        String id = req.getParameter("id");
        try {
            MatchService matchService = factory.getService(MatchService.class);
            Match match = matchService.getMatchById(Integer.parseInt(id));
            req.setAttribute("match", match);

            return new Forward("bet.jsp");
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(), e);
            return new Forward("error.jsp");
        }

    }
}
