package by.epam.makarevich.totalizator.dao.constants;

public class MatchConstant {
    public static final String ID = "id";
    public static final String FIRST_TEAM_ID = "first_team_id";
    public static final String SECOND_TEAM_ID = "second_team_id";
    public static final String DATE = "match_date";
    public static final String RESULT = "result_common";
    public static final String FIRST_TEAM_SCORE = "result_first_team";
    public static final String SECOND_TEAM_SCORE = "result_second_team";
    public static final String MATCH_STATUS = "match_status";

    public static final String ADMIN = "cash_amount";
    public static final String FIRST_TEAM_NAME = "T1Name";
    public static final String SECOND_TEAM_NAME = "T2Name";
    public static final String SPORT = "sport";

    public static final double WIN_COEFFICIENT = 1.5;


}
