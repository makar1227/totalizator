package by.epam.makarevich.totalizator.command;

import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorUserServiceException;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.interfaces.UserService;
import by.epam.makarevich.totalizator.util.*;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class RegistrationCommand extends Command {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        Command.Forward fail = new Command.Forward("/registration.jsp");
        boolean create = false;

        req.setAttribute("error", fail);

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String passwordConfirm = req.getParameter("passwordConfirm");
        String email = req.getParameter("email");
        String birthDate = req.getParameter("birth");
        String cash = req.getParameter("cash");
        String phone = req.getParameter("phone");

        try {
            UserService userService = factory.getService(UserService.class);
            User user = new User();
            user.setLogin(login);

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date d = sdf.parse(birthDate);
                user.setBirth(d);
            } catch (ParseException e) {
                fail.getAttributes().put("birthError", Constant.ERROR_REGISTER_BIRTH);
            }
            if (PasswordValidator.checkPassword(password)) {
                if (password.equals(passwordConfirm)) {
                    user.setPassword(password);
                } else {
                    user.setPassword(password);
                    fail.getAttributes().put("passwordError", Constant.ERROR_REGISTER_DIFFERENT_PASSWORDS);
                }
            } else {
                fail.getAttributes().put("simplePasswordError", Constant.ERROR_REGISTER_SIMPLE_PASSWORDS);
            }

            if (EmailValidator.validateEmail(email)) {
                user.setEmail(email);
            } else {
                fail.getAttributes().put("emailError", Constant.ERROR_REGISTER_INCORRECT_EMAIL);

            }
            if (PhoneValidator.checkPhone(phone)) {
                user.setPhone(phone);
            } else {
                fail.getAttributes().put("phoneError", Constant.ERROR_REGISTER_INCORRECT_PHONE);

            }
            if (CashValidator.checkCash(cash)) {
                user.setCashAmount(Double.valueOf(cash));
            } else {
                fail.getAttributes().put("cashError", Constant.ERROR_REGISTER_CASH);

            }
            if (FailChecker.checkFail(fail)) {
                try {
                    create = userService.addUser(user);
                } catch (TotalizatorUserServiceException e) {
                    switch (e.getCode()) {
                        case TotalizatorUserServiceException.NOT_UNIQUE_USER:
                            fail.getAttributes().put("loginError", e.getMessage());
                            break;
                        case TotalizatorUserServiceException.EMAIL_EXISTS:
                            fail.getAttributes().put("emailError", e.getMessage());
                            break;
                        case TotalizatorUserServiceException.WRONG_BIRTH_DATE:
                            fail.getAttributes().put("birthError", e.getMessage());
                            break;
                    }
                }
            }

            if (FailChecker.checkFail(fail) && create) {
                return new Forward("/login.jsp");
            }
            req.setAttribute("user", user);
            return fail;

        } catch (TotalizatorServiceException e) {
            logger.fatal("Exception occurred in database", e);
            return new Forward("error.jsp");
        }
    }
}
