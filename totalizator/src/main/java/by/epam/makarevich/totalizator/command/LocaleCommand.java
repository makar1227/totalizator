package by.epam.makarevich.totalizator.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LocaleCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        String locale = req.getParameter("locale");
        HttpSession session = req.getSession();
        session.setAttribute("locale", locale);
        return new Forward("/", true);
    }
}
