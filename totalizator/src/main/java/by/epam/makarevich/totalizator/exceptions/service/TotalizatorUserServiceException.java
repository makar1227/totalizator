package by.epam.makarevich.totalizator.exceptions.service;

/**
 * Created by makar1227 on 5/11/2017.
 */
public class TotalizatorUserServiceException extends TotalizatorServiceException {
    public static final int WRONG_BIRTH_DATE = 1;
    public static final int NOT_UNIQUE_USER = 2;
    public static final int EMAIL_EXISTS = 3;
    public static final int INTERNAL_ERROR = 4;
    private int code;

    public TotalizatorUserServiceException(Throwable cause) {
        super(cause);
    }

    public TotalizatorUserServiceException(int code) {
        this.code = code;
    }
    public TotalizatorUserServiceException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
