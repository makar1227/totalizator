package by.epam.makarevich.totalizator.service.impl;

import by.epam.makarevich.totalizator.command.constant.Constant;
import by.epam.makarevich.totalizator.dao.interfaces.UserDao;
import by.epam.makarevich.totalizator.exceptions.dao.TotalizatorSqlException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorUserServiceException;
import by.epam.makarevich.totalizator.model.User;
import by.epam.makarevich.totalizator.service.interfaces.UserService;
import by.epam.makarevich.totalizator.util.AdultChecker;
import by.epam.makarevich.totalizator.util.Crypthographic;

import java.security.NoSuchAlgorithmException;

/**
 * The type User service.
 */
public class UserServiceImpl extends ServiceImpl implements UserService {

    @Override
    public User getUserById(int id) throws TotalizatorServiceException {
        try {
            return factory.getUserDao().getUserById(id);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }

    @Override
    public boolean addUser(User user) throws TotalizatorServiceException {
        try {
            UserDao userDao = factory.getUserDao();

            if (userDao.getUserByLogin(user.getLogin()) != null) {
                throw new TotalizatorUserServiceException(Constant.ERROR_REGISTER_EXISTING_LOGIN, TotalizatorUserServiceException.NOT_UNIQUE_USER);
            }
            if (getUserByEmail(user.getEmail()) != null) {
                throw new TotalizatorUserServiceException(Constant.ERROR_REGISTER_EXISTING_EMAIL, TotalizatorUserServiceException.EMAIL_EXISTS);
            }
            if (!AdultChecker.checkAdult(user.getBirth())) {
                throw new TotalizatorUserServiceException(Constant.ERROR_REGISTER_UNDERAGE, TotalizatorUserServiceException.WRONG_BIRTH_DATE);
            }
            user.setPassword(Crypthographic.getHash(user.getPassword()));
            userDao.addUser(user);

        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in User service layer.", e);
        } catch (NoSuchAlgorithmException e) {
            throw new TotalizatorUserServiceException("Internal error", TotalizatorUserServiceException.INTERNAL_ERROR);
        }
        return true;
    }

    @Override
    public void editUser(User user) throws TotalizatorServiceException {
        try {
            UserDao userDao = factory.getUserDao();
            User tempUser = getUserByEmail(user.getEmail());

            if (tempUser != null && tempUser.getId() != user.getId()) {
                throw new TotalizatorUserServiceException(Constant.ERROR_REGISTER_EXISTING_EMAIL, TotalizatorUserServiceException.EMAIL_EXISTS);
            }
            user.setPassword(Crypthographic.getHash(user.getPassword()));

            userDao.editUser(user);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in User service layer.", e);
        } catch (NoSuchAlgorithmException e) {
            throw new TotalizatorUserServiceException("Internal error", TotalizatorUserServiceException.INTERNAL_ERROR);
        }
    }

    @Override
    public User getUserByLogin(String login) throws TotalizatorServiceException {
        try {
            return factory.getUserDao().getUserByLogin(login);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorUserServiceException("Internal error", TotalizatorUserServiceException.INTERNAL_ERROR);
        }
    }

    @Override
    public User getUserByEmail(String email) throws TotalizatorServiceException {
        try {
            return factory.getUserDao().getUserByEmail(email);
        } catch (TotalizatorSqlException e) {
            throw new TotalizatorServiceException("Exception in service layer.", e);
        }
    }
}
