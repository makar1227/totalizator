package by.epam.makarevich.totalizator.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The type Crypthographic.
 */
public class Crypthographic {

    /**
     * Gets hash.
     *
     * @param password the password
     * @return the hash
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public static String getHash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    /**
     * Check hash boolean.
     *
     * @param hash     the hash
     * @param password the password
     * @return the boolean
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public static boolean checkHash(String hash, String password) throws NoSuchAlgorithmException {
        return hash.equals(getHash(password));
    }


}