package by.epam.makarevich.totalizator.command.admin;

import by.epam.makarevich.totalizator.command.Command;
import by.epam.makarevich.totalizator.exceptions.service.TotalizatorServiceException;
import by.epam.makarevich.totalizator.model.Match;
import by.epam.makarevich.totalizator.service.interfaces.MatchService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MatchEditPageCommand extends Command {
    @Override
    public Forward exec(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String id = req.getParameter("id");
            System.out.println("maich id= "+id);
            MatchService matchService = factory.getService(MatchService.class);
            Match match = matchService.getMatchById(Integer.parseInt(id));

            req.setAttribute("match", match);
            return new Forward("/admin/matchEdit.jsp");
        } catch (TotalizatorServiceException e) {
            logger.error(e.getMessage(),e);
            return new Forward("/error.jsp");
        }
    }
}
